package titech;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import static titech.util.EasyGetter.*;

public class LogConfig {
	
	private String CFGEXT = "cfg";
	private String DOCID = "doc";
	private String WHOID = "who";
	private String DATE = "date";
	private Properties prop = new Properties();
	
	public LogConfig(String logdirpath) {
		try {
			prop.load(new FileInputStream(getAppManager().findFile(CFGEXT)));
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(-1);
		}
	}
	
	public String getDocid() {
		return prop.getProperty(DOCID);
	}
	
	public String getWhoid() {
		return prop.getProperty(WHOID);
	}
	
	public String getDate() {
		return prop.getProperty(DATE);
	}
}
