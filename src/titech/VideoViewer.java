package titech;

import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Set;

import titech.action.Annotation;
import titech.loader.ActFileLoader;
import titech.loader.EyeFileLoader;
import titech.manager.LogManager;
import static titech.util.EasyGetter.*;

import com.googlecode.javacv.CanvasFrame;
import com.googlecode.javacv.OpenCVFrameGrabber;
import static com.googlecode.javacv.cpp.opencv_core.*;
import static com.googlecode.javacv.cpp.opencv_highgui.*;

public class VideoViewer {

	private static final int CORRECT_ARGS_LENGTH = 1;
	
	private File act;
	private File eye;
	private File video;
	private String logDirPath;
	
	private long captureStartTime;
	private long trimTime;
	
	private OpenCVFrameGrabber grabber = null;
	private CanvasFrame canvas = null;

	public static void main(String[] args) {
		try {
			if (args.length != CORRECT_ARGS_LENGTH)
				throw new Exception("Error args");
			
			VideoViewer app = new VideoViewer(args[0]);
			app.run();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("Finish.");
	}

	public VideoViewer(String logDirPath) throws InterruptedException,
			com.googlecode.javacv.FrameGrabber.Exception {
		
		this.logDirPath = logDirPath;
		
		getAppManager().initialize(logDirPath, false);
		video = getAppManager().findFile("avi");
		
		captureStartTime = getMsFromFile(video);
		trimTime = getActionManager().getAnnoStartTime() - captureStartTime;
	}
	
	public void run() throws com.googlecode.javacv.FrameGrabber.Exception {
		grabber = new OpenCVFrameGrabber(video);
		grabber.start();
		
		// play();
		snap();
	}
	
	private void snap() {
//		canvas = new CanvasFrame(video.getName());
//		canvas.setDefaultCloseOperation(javax.swing.JFrame.EXIT_ON_CLOSE);
		
		setAllOutToLog();
		
		try {
			for (Annotation a : getActionManager().getAlteringAnnotations()) {
				long time = trimTime + a.getTime();
				grabber.setTimestamp(time * 1000);
				IplImage image = grabber.grab();
				
				System.out.println(getPrettyTime(time) + " " + a.toString());
				putText(image, getPrettyTime(time) + " (" +
						String.format("%.3f", a.getTimeInUTC() / 1000.0) + ")");
				saveImage(image, a.getTimeInUTC() + "");
				
				if (canvas != null) {
					canvas.showImage(image);
					KeyEvent key = canvas.waitKey(10);
					if (key != null) break;
				} else {
					// Do NOT Delete this for saving image.
					Thread.sleep(10);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void play() {
		canvas = new CanvasFrame(video.getName());
		canvas.setDefaultCloseOperation(javax.swing.JFrame.EXIT_ON_CLOSE);
		
		final long allTime = grabber.getLengthInTime() / 1000;
		final int incTime = 1000;
		
		System.out.printf("Video length is %02d:%02d. ",
				allTime / 1000 / 60, allTime / 1000 % 60);
		System.out.printf("Trimed time is %02d:%02d.\n",
				trimTime / 1000 / 60, trimTime / 1000 % 60);

		try {
			for (long time = trimTime; time < allTime; time += incTime) {
				grabber.setTimestamp(time * 1000);
				IplImage image = grabber.grab();
				putText(image, getPrettyTime(time));
				canvas.showImage(image);
				
				int waitTime = 1;
				if ((time-trimTime) % 10000 == 0) waitTime = incTime;
				KeyEvent key = canvas.waitKey(waitTime);
				if (key != null) break;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void putText(IplImage image, String text) {
		cvPutText(image, text, new CvPoint(70, 15),
				new CvFont(CV_FONT_HERSHEY_PLAIN, 1, 1),
				cvScalar(0, 0, 0, 0));
	}
	
	private void saveImage(IplImage image, String name) {
		touchSnapDir();
		cvSaveImage(getSnapPath(name), image,
				new int[] {CV_IMWRITE_JPEG_QUALITY, 80});
		try {
			Thread.sleep(200);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	private void setAllOutToLog() {
		touchSnapLog();
		try {
			System.setOut(new PrintStream(new FileOutputStream(getSnapLogPath())));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.exit(-1);
		}
	}
	
	private void touchSnapDir() {
		File snapDir = new File(getAppManager().getSnapDirPath());
		if (!snapDir.exists()) snapDir.mkdir();
	}
	
	private void touchSnapLog() {
		try {
			touchSnapDir();
			File snapLog = new File(getSnapLogPath());
			if (!snapLog.exists()) snapLog.createNewFile();
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(-1);
		}
	}
	
	private String getSnapPath(String name) {
		if (!name.endsWith(".jpg")) name += ".jpg";
		return getAppManager().getSnapDirPath() + File.separator + name;
	}
	
	public String getSnapLogPath() {
		return getAppManager().getSnapDirPath() + File.separator +
				AppConfig.SNAP_LOG_NAME;
	}
	
	// File name example: 20121025_1609_01.suffix
	private long getMsFromFile(File file) {
		String[] s = file.getName().split("_");

		final int year = Integer.parseInt(s[0].substring(0, 4));
		final int month = Integer.parseInt(s[0].substring(4, 6)) - 1;
		final int date = Integer.parseInt(s[0].substring(6, 8));
		final int hour = Integer.parseInt(s[1].substring(0, 2));
		final int minute = Integer.parseInt(s[1].substring(2));
		final int second = Integer.parseInt(s[2].substring(0, 2));

//		System.out.println("Capture started at " + year + "." + (month+1) +
//				"." + date + "." + hour + "." + minute + "." + second + ".");

		Calendar ret = Calendar.getInstance();
		ret.set(year, month, date, hour, minute, second);
		ret.set(Calendar.MILLISECOND, 0);
		return ret.getTimeInMillis();
	}
	
	private String getPrettyTime(long addTime) {
		Calendar c = Calendar.getInstance();
		c.setTimeInMillis(captureStartTime + addTime);
		return String.format(
				"%02d:%02d:%02d", c.get(Calendar.HOUR_OF_DAY),
				c.get(Calendar.MINUTE), c.get(Calendar.SECOND));
	}
}
