package titech;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.geom.Line2D;
import java.awt.geom.RoundRectangle2D;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import javax.imageio.ImageIO;

import titech.util.HTMLColor;

class Pair<T1, T2> {
	T1 first;
	T2 second;
	public Pair(T1 a, T2 b) {
		this.first = a;
		this.second = b;
	}
}

class Pca {
	static private HashMap<String, String> pretty = new HashMap<String, String>() {{
		put("ga", "ガ");
		put("o", "ヲ");
		put("ni", "ニ");
	}};
	String pred, cas, arg;
	int nullid;
	public Pca(String pred, String cas, String arg, int nullid) {
		this.pred = pred;
		this.cas = pretty.get(cas);
		this.arg = arg;
		this.nullid = nullid;
	}
}

class NullColor {
	private static String[] colors = { "crimson", "orangered", "darkcyan", "darkmagenta" };
	private static int pos = 0;
	public static Color get() {
		Color ret = HTMLColor.get(colors[pos]);
		++pos;
		pos %= colors.length;
		return ret;
	}
	public static void reset() {
		pos = 0;
	}
}

public class NullDrawer {
	
	static private String PICEXT = "jpg";
	
	// セグメントの形
	TreeMap<String, Shape> seg2rect = new TreeMap<String, Shape>();
	
	// ログごとに可視化するセグメントのペア
	TreeMap<String, ArrayList<Pca>> log2vis = new TreeMap<String, ArrayList<Pca>>();
	
	// 述語ごとのnullの個数
	TreeMap<String, Integer> pred2nullc = null;
	
	// 項ごとのnullの個数
	TreeMap<String, Integer> arg2nullc = null;
	
	public NullDrawer() {
	}
	
	public static void main(String argv[]) {
		NullDrawer nd = new NullDrawer();
		
		nd.loadSegs("segs.txt");
		nd.loadNulls("null.vis");
		nd.visualize();
	}
	
	private void visualize() {
		for (Map.Entry<String, ArrayList<Pca>> e : log2vis.entrySet()) {
			String logdirpath = e.getKey();
			ArrayList<Pca> pcas = e.getValue();
			ArrayList<File> snapfiles = getSnaps(logdirpath);
			
			
			// ログごとに出力ディレクトリを作成する
			System.out.print(logdirpath);
			touchDestdir(getDestdirpath(logdirpath));
			
			for (File snapfile : snapfiles) {
				
				// スナップファイルごとに初期化する
				NullColor.reset();
				pred2nullc = new TreeMap<String, Integer>();
				arg2nullc = new TreeMap<String, Integer>();
				
				try {
					BufferedImage snap = ImageIO.read(snapfile);
					
					for (Pca pca : pcas) {
						String pred = pca.pred;
						String cas = pca.cas;
						String arg = pca.arg;
						String pupper = "" + pca.nullid + "(" + getBody(arg) + ":" + cas + ")";
						String aupper = "" + pca.nullid;
						Color col = NullColor.get();
						
						updateCount(pred, arg);
						
						int poffset = 15 * (pred2nullc.get(pred) - 1);
						int aoffset = 15 * (arg2nullc.get(arg) - 1);
						
						paint(snap, (RoundRectangle2D)seg2rect.get(pred), pupper, col, poffset);
						paint(snap, (RoundRectangle2D)seg2rect.get(arg), aupper, col, aoffset);
					}
					
					String path = getDestdirpath(logdirpath) + snapfile.getName();
					
					saveImage(snap, path);
					
					System.out.print(".");
					
				} catch (IOException exception) {
					exception.printStackTrace();
					System.exit(-1);
				}
			}
			
			System.out.println("done!");
			//break;
		}
	}
	
	public void updateCount(String pred, String arg) {
		if (pred2nullc.containsKey(pred)) {
			int nextc = pred2nullc.get(pred) + 1;
			pred2nullc.put(pred, nextc);
		} else {
			pred2nullc.put(pred, 1);
		}
		
		if (arg2nullc.containsKey(arg)) {
			int nextc = arg2nullc.get(arg) + 1;
			arg2nullc.put(arg, nextc);
		} else {
			arg2nullc.put(arg, 1);
		}
	}
	
	public void paint(BufferedImage image, RoundRectangle2D rect, String upper, Color col, int offset) {
		
		Graphics2D g = image.createGraphics();
		BasicStroke stroke = new BasicStroke(3.0f);
		
		int ax = (int)rect.getX();
		int ay = (int)rect.getY() - 3 - offset;
		int w = (int)rect.getWidth();
		
		Shape line = new Line2D.Double(ax, ay + 2, ax + w, ay + 2);
		
	    g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g.setStroke(stroke);
		g.setPaint(col);
		if (!upper.isEmpty()) g.drawString(upper, ax, ay);
		g.draw(line);
		g.drawImage(image, 0, 0, null);
	}
	
	private String getBody(String seg) {
		return seg.split(",")[4];
	}
	
	// 画像ファイル一覧を取得する
	private ArrayList<File> getSnaps(String path) {
		ArrayList<File> ret = new ArrayList<File>();
		
		for (File f : new File(path).listFiles())
			if (f.getName().endsWith(PICEXT))
				ret.add(f);
		
		return ret;
	}	
	
	// 画像を保存する
	public void saveImage(BufferedImage image, String path) {
		try {
			ImageIO.write(image, PICEXT, new File(path));
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(-1);
		}
	}
	
	public String getDestdirpath(String logdirpath) {
		return logdirpath + "snaps2/";
	}
	
	public boolean touchDestdir(String path) {
		File d = new File(path);
		return d.mkdirs();
	}
	
	// セグメントの形をファイルから読み込む
	private void loadSegs(String path) {
		try {
			BufferedReader br = new BufferedReader(new FileReader(path));
			String line;
			while ((line = br.readLine()) != null) {
				String cols[] = line.split("\t");
				String seg = cols[0];
				String[] cords = cols[1].split(",");
				int x = Integer.parseInt(cords[0]);
				int y = Integer.parseInt(cords[1]);
				int w = Integer.parseInt(cords[2]);
				int h = Integer.parseInt(cords[3]);
				
				seg2rect.put(seg, new RoundRectangle2D.Double(x-2, y-2, w+2, h+4, 8, 8));
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(-1);
		}
	}
	
	// 可視化するnullをファイルから読み込む
	private void loadNulls(String path) {
		try {
			BufferedReader br = new BufferedReader(new FileReader(path));
			String line;
			while ((line = br.readLine()) != null) {
				String cols[] = line.split("\t");
				
				int nullid = Integer.parseInt(cols[0]);
				
				String pca[] = cols[1].split("_");
				String pred = pca[0];
				String cas = pca[1];
				String arg = pca[2];
				
				String logdirpath = cols[2];
				
				if (!log2vis.containsKey(logdirpath))
					log2vis.put(logdirpath, new ArrayList<Pca>());
				
				log2vis.get(logdirpath).add(new Pca(pred, cas, arg, nullid));
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(-1);
		}
	}
}
