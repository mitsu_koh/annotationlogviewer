package titech.ui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.EtchedBorder;
import javax.swing.border.LineBorder;

import titech.action.Annotation;
import titech.action.Fixation;
import titech.manager.ActionManager;
import titech.util.ExtArrayList;
import static titech.util.EasyGetter.*;

public class InfoPanel extends JPanel {
	
	public static final int BAR_MAX_VAL = 100;
	
	private static final int PANEL_WIDTH = 200;
	private static final int LINE_LABEL_HEIGHT = 30;
	private static final int ACTION_LIST_AREA_HEIGHT = 200;
	private static final String FIXATION_NUM = "5";
	private static final String PLAY_SPEED = "1.0";
	private static final int ACTION_LIST_NUM = 10;
	
	private JTextField timeField;
	private JTextField fixationField;
	private JTextField fixationNumFiled;
	private JTextField playSpeedField;
	private JProgressBar annotateBar;
	private JProgressBar nextAnnoBar;
	private JTextField nextAltAnno;
	private AnnoPanel annoPanel;
	
	private TimePanel timePanel;
	private ExtArrayList<Annotation> annotations;
	
	public InfoPanel(TimePanel timePanel, int height) {
		this.timePanel = timePanel;
		this.annotations = new ExtArrayList<Annotation>(getActionManager().getAlteringAnnotations());
		
		timeField = new JTextField("0.0");
		fixationField = new JTextField("");
		fixationNumFiled = new JTextField(FIXATION_NUM);
		playSpeedField = new JTextField(PLAY_SPEED);
		annotateBar = new JProgressBar(0);
		nextAnnoBar = new JProgressBar(0);
		nextAltAnno = new JTextField("");
		annoPanel = new AnnoPanel(timePanel, PANEL_WIDTH);
		
		Dimension cd = new Dimension(PANEL_WIDTH, LINE_LABEL_HEIGHT);
		timeField.setPreferredSize(cd);
		fixationField.setPreferredSize(cd);
		fixationNumFiled.setPreferredSize(cd);
		playSpeedField.setPreferredSize(cd);
		annotateBar.setPreferredSize(cd);
		nextAnnoBar.setPreferredSize(cd);
		nextAltAnno.setPreferredSize(cd);
		
		timeField.setEditable(false);
		fixationField.setEditable(false);
		nextAltAnno.setEditable(false);
		
		renew();
		
		setPreferredSize(new Dimension(PANEL_WIDTH, height));
		setLayout(new FlowLayout());
		
		add(new JLabel("Time (sec)"));
		add(timeField);
		add(new JLabel("Duration (sec)"));
		add(fixationField);
		add(new JLabel("Point Num"));
		add(fixationNumFiled);
		add(new JLabel("Play Speed"));
		add(playSpeedField);
		add(new JLabel("Next Annotation"));
		add(nextAltAnno);
		add(new JLabel("Creating (%)"));
		add(annotateBar);
		add(new JLabel("Until Next Annotation (%)"));
		add(nextAnnoBar);
		add(annoPanel);
		
		adjustSize();
	}
	
	public void renew() {
		renewTimeLabel();
		renewTrackLabel();
		renewAnnotateBar();
		renewNextAnnoBar();
		renewNextAltAnno();
		annoPanel.reflesh();
	}
	
	public void adjustSize() {
		annoPanel.adjustSize();
	}
	
	private void renewTimeLabel() {
		timeField.setText(timePanel.getMsTime() / 1000.0 + "");
	}
	
	private void renewTrackLabel() {
		String text = "TODO";
		int[] section = ActionManager.instance().getFixationSection(
				timePanel.getMsTime());
		for (int i = section[0]; i < section[1]; ++i) {
			Fixation f = ActionManager.instance().getFixations().get(i);
			if (f.compareTo(new Fixation(timePanel.getMsTime())) == 0) {
				text = "" + f.getDuration() / 1000.0;
				break;
			}
		}
		fixationField.setText(text);
	}
	
	private void renewAnnotateBar() {
		int ratio = getActionManager().getAnnoProgress(timePanel.getMsTime());
		annotateBar.setValue(ratio);
	}
	
	private void renewNextAnnoBar() {
		int ratio = getActionManager().getNextAnnoRatio(timePanel.getMsTime());
		nextAnnoBar.setValue(ratio);
	}
	
	private void renewNextAltAnno() {
		int next = annotations.upperBound(new Annotation(timePanel.getMsTime()));
		if (next >= annotations.size()) next = annotations.size()-1;
		nextAltAnno.setText(annotations.get(next).toPrettyStr());
	}
	
	public int getFixationNum() {
		return Integer.parseInt(fixationNumFiled.getText());
	}
	
	public double getPlaySpeed() {
		return Double.parseDouble(playSpeedField.getText());
	}
}
