package titech.ui;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;

import javax.swing.AbstractButton;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.Timer;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import titech.manager.ShapeManager;
import titech.manager.UiManager;
import static titech.util.EasyGetter.*;

public class UtilPanel extends JPanel {

	public static String PLAY = "Play";
	public static String STOP = "Stop";
	public static String APPLY = "Apply";
	public static String DRAW = "Draw";
	public static String ADJUST = "Adjust";

	private TreeMap<String, AbstractButton> buttons;

	private JButton playButton;
	private JButton stopButton;
	private JButton applyButton;
	private JCheckBox drawCheckBox;
	private JCheckBox adjCheckBox;

	public UtilPanel() {

		playButton = new JButton(PLAY);
		stopButton = new JButton(STOP);
		applyButton = new JButton(APPLY);
		drawCheckBox = new JCheckBox(DRAW, getShapeManager().isDrawing());
		adjCheckBox = new JCheckBox(ADJUST, getShapeManager().isAdjusting());

		super.add(drawCheckBox);
		super.add(adjCheckBox);
		super.add(playButton);
		super.add(stopButton);
		super.add(applyButton);

		buttons = new TreeMap<String, AbstractButton>();
		buttons.put(PLAY, playButton);
		buttons.put(STOP, stopButton);
		buttons.put(APPLY, applyButton);
		buttons.put(DRAW, drawCheckBox);
		buttons.put(ADJUST, adjCheckBox);

		for (Map.Entry<String, AbstractButton> e : buttons.entrySet()) {
			AbstractButton b = e.getValue();
			b.addActionListener(UiManager.instance());
			b.setActionCommand(e.getKey());
			b.setMaximumSize(new Dimension(Short.MAX_VALUE, Short.MAX_VALUE));
		}

		playButton.setEnabled(true);
		stopButton.setEnabled(false);

		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
	}

	public void setPlayEnabled(boolean enabled) {
		playButton.setEnabled(enabled);
	}

	public void setStopEnabled(boolean enabled) {
		stopButton.setEnabled(enabled);
	}
}
