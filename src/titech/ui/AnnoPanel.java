package titech.ui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.FontMetrics;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.border.EtchedBorder;

import titech.action.Action;
import titech.manager.ActionManager;
import static titech.util.EasyGetter.*;

public class AnnoPanel extends JPanel {
	
	private JLabel title;
	private JScrollPane scrollPane;
	private JTextArea actionList;
	
	private TimePanel timePanel;
	
	public AnnoPanel(TimePanel timePanel, int width) {
		this.timePanel = timePanel;
		
		title = new JLabel("Actions");
		
		String init = initString();
		actionList = new JTextArea(init);
		actionList.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.GRAY));
		actionList.setEditable(false);
		
		scrollPane = new JScrollPane(actionList,
				JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
				JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		
		setPreferredSize(new Dimension(width, Short.MAX_VALUE));
		setLayout(new FlowLayout());
		add(title);
		add(scrollPane);
	}
	
	public void reflesh() {
		refleshAnnoActList();
	}
	
	public void adjustSize() {
		int pWidth = getParent().getWidth();
		int pHeight = getParent().getHeight();
		int y = getY();
		int height = pHeight - y;
		scrollPane.setPreferredSize(
				new Dimension(pWidth, height-title.getHeight()-10));
		setPreferredSize(new Dimension(pWidth, height));
		revalidate();
		repaint();
	}
	
	private String initString() {
		ArrayList<Action> actions = new ArrayList<Action>();
		for (Action a : getActionManager().getAnnotations()) actions.add(a);
		// for (Action a : getActionManager().getFixations()) actions.add(a);
		Collections.sort(actions);
		
		String ret = "";
		for (int i = 0; i < actions.size(); ++i) {
			String append = "   " + actions.get(i).toPrettyStr() + "\n";
			ret += append;
		}
		return ret;
	}
	
	
	private void refleshAnnoActList() {
		
	}
}
