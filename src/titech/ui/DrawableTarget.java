package titech.ui;

import titech.manager.ShapeManager;
import titech.shape.*;

public interface DrawableTarget {
	public void drawBy(ShapeManager drawer);
	public void clear();
	public void flush();

	public void draw(Arc a);
	public void draw(Rectangle r);
	public void draw(Line l);
	public void draw(Circle c);
	public void draw(Char c);
	public void draw(Str s);
}
