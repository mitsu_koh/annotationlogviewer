package titech.ui;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JPanel;

import titech.manager.ShapeManager;
import titech.shape.Arc;
import titech.shape.Char;
import titech.shape.Circle;
import titech.shape.Line;
import titech.shape.Rectangle;
import titech.shape.Str;

public class MonitorPanel extends JPanel implements DrawableTarget {
	
	private final static Color DEFAULT_COLOR = new Color(0, 0, 0);
	private final static BasicStroke DEFAULT_STROKE = new BasicStroke(2.0f);
	private final static BasicStroke WIDE_STROKE = new BasicStroke(4.0f);
	
	private SnapContainer snaps;
	
	private Graphics2D graphics;
	private BufferedImage snap;

	public MonitorPanel(int width, int height, ArrayList<File> snaps) {
		this.snaps = new SnapContainer(snaps);

		setPreferredSize(new Dimension(width, height));
		setSize(new Dimension(width, height));
		setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.GRAY));
		clear(0);
	}

	public void paintComponent(Graphics g) {
		super.paintComponents(g);
		synchronized(this) {
			g.drawImage(snap, 0, 0, null);
		}
	}

	public void setColor(Color color) {
		graphics.setColor(color);
	}

	@Override
	public void flush() {
		repaint();
	}

	@Override
	public void clear() throws UnsupportedOperationException {
		throw new UnsupportedOperationException();
	}

	public void clear(long time) {
		snap = snaps.getSnap(time, getWidth(), getHeight());
		graphics = snap.createGraphics();
		graphics.drawImage(snap, 0, 0, this);
		graphics.setColor(DEFAULT_COLOR);
		graphics.setStroke(DEFAULT_STROKE);
	}

	@Override
	public void drawBy(ShapeManager drawer) {
		drawer.draw(this);
	}

	@Override
	public void draw(Rectangle r) {
		int m = 10;
		Color bak = graphics.getColor();
		graphics.setColor(r.getAttr().getColor());
		graphics.setStroke(WIDE_STROKE);
		if (r.getAttr().isFilled())
			graphics.fillRect(X(r.getX() - m), Y(r.getY() - m), X(r.getW() + m * 2), Y(r.getH()) + m * 2);
		else
			graphics.drawRect(X(r.getX() - m), Y(r.getY() - m), X(r.getW() + m * 2), Y(r.getH()) + m * 2);
		graphics.setColor(bak);
	}

	@Override
	public void draw(Line l) {
		Color bak = graphics.getColor();
		graphics.setStroke(DEFAULT_STROKE);
		graphics.setColor(l.getAttr().getColor());
		graphics.drawLine(X(l.getAx()), Y(l.getAy()), X(l.getBx()), Y(l.getBy()));
		graphics.setColor(bak);
	}

	@Override
	public void draw(Circle c) {
		Color bak = graphics.getColor();
		final int r = c.getR();
		final int tlx = c.getX() - r;
		final int tly = c.getY() - r;
		graphics.setColor(c.getAttr().getColor());
		graphics.setStroke(DEFAULT_STROKE);
		graphics.drawArc(X(tlx), Y(tly), X(r*2), Y(r*2), 0, 360);
		graphics.setColor(bak);
	}

	@Override
	public void draw(Arc a) {
	}

	@Override
	public void draw(Char c) {
	}

	@Override
	public void draw(Str s) {
	}

	private int X(int x) {
		return (int)(1.0 * x * getWidth() / snaps.getWidth());
	}

	private int Y(int y) {
		return (int)(1.0 * y * getHeight() / snaps.getHeight());
	}
}
