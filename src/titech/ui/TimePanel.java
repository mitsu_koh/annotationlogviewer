package titech.ui;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.TreeMap;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.Timer;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import titech.action.Annotation;
import titech.action.Glance;
import titech.manager.ActionManager;
import titech.manager.UiManager;

public class TimePanel extends JPanel {

	public static final String TIMER = "Timer";
	public static final int DEFAULT_TIMER_DELAY_MS = 100;
	public static final double DEFAULT_PLAY_SPEED = 1.0;
	public static final int TICK_SPACE_MS = 100 * 1000;

	private JSlider slider;
	private JLabel actionLabel;
	private Timer timer;
	private int msTime;
	private int minMs;
	private int maxMs;
	
	private int timerDelayMs;
	private double playSpeed;

	public TimePanel(int min, int max) {
		timerDelayMs = DEFAULT_TIMER_DELAY_MS;
		playSpeed = DEFAULT_PLAY_SPEED;
		
		slider = new JSlider(min, max, 0);
		slider.addChangeListener(UiManager.instance());
		setMajor();
		minMs = min;
		maxMs = max;

		setSliderLabel();

		actionLabel = new JLabel();

		slider.setMaximumSize(new Dimension(Short.MAX_VALUE, Short.MAX_VALUE));
		actionLabel.setMaximumSize(new Dimension(Short.MAX_VALUE, Short.MAX_VALUE));

		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		super.add(slider);
		super.add(actionLabel);

		timer = new Timer(timerDelayMs, UiManager.instance());
		timer.setActionCommand(TIMER);
	}

	private void setMajor() {
		slider.setMajorTickSpacing(TICK_SPACE_MS);
		slider.setPaintTicks(true);
	}

	public void setSliderLabel() {
		Hashtable<Integer, JLabel> labels = new Hashtable<Integer, JLabel>();
		for (int i = minMs; i < maxMs; i += TICK_SPACE_MS)
			labels.put(new Integer(i), new JLabel(i/1000 + ""));
		slider.setLabelTable(labels);
		slider.setPaintLabels(true);
	}

	public void startTimer() {
		timer.start();
	}

	public void stopTimer() {
		timer.stop();
	}

	public void setActionLabel(Annotation aa) {
		actionLabel.setText(aa.toString());
	}
	
	public void addMsTime(int add) {
		add = (int)(add * playSpeed);
		setMsTime(msTime + add);
	}

	//--------------------------------------------------
	// Getters and Setters

	public int getMsTime() {
		return msTime;
	}

	public void setMsTime(int time) {
		this.msTime = time;
		slider.setValue(time);
	}

	public int getMaxMsTimestamp() {
		return maxMs;
	}

	public int getMinMsTimestamp() {
		return minMs;
	}

	public int getTimerDelayMs() {
		return timerDelayMs;
	}

	public void setTimerDelayMs(int timerDelayMs) {
		this.timerDelayMs = timerDelayMs;
	}

	public double getPlaySpeed() {
		return playSpeed;
	}

	public void setPlaySpeed(double playSpeed) {
		this.playSpeed = playSpeed;
	}
}