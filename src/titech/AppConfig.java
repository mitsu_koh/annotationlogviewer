package titech;

import java.io.File;

public class AppConfig {
	public static final int TOBII_WIDTH_PX = 1280;
	public static final int TOBII_HEIGHT_PX = 1024;
	public static final File SEG_LIST_FILE = new File("./load/segment_v3.dump");
	public static final String FIXATION_LOG_PATH = "./result/fix.txt";
	public static final String FIX_LIST_LOG_PATH = "./result/fixes.txt";
	public static final String WORK_TIME_LOG_PATH = "./result/time.txt";
	public static final String SNAP_DIR_NAME = "snaps2";
	public static final String SNAP_LOG_NAME = "snaplog.txt";
	
	public static Integer fontSize = null;
	
	public static boolean checkPoint(int x, int y) {
		return (0 <= x && x < TOBII_WIDTH_PX
				&& 0 <= y && y < TOBII_HEIGHT_PX);
	}
}
