package titech.region;

public abstract class Region {
	int x;
	int y;
	int w;
	int h;

	public Region(int tlx, int tly, int brx, int bry) {
		this.x = tlx;
		this.y = tly;
		this.w = brx - tlx + 1;
		this.h = bry - tly + 1;
	}
	
	public int getMidX() {
		return x + w / 2;
	}
	
	public int getMidY() {
		return y + h / 2;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getW() {
		return w;
	}

	public void setW(int w) {
		this.w = w;
	}

	public int getH() {
		return h;
	}

	public void setH(int h) {
		this.h = h;
	}
}
