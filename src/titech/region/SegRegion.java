package titech.region;

import java.util.ArrayList;

import titech.annotation.Segment;
import titech.shape.Rectangle;
import titech.shape.Shape;
import titech.shape.ShapeAttribute;

public class SegRegion extends Rectangle implements Comparable<Shape> {
	
	private final int WIDTH_ADJUST = 0;
	private final int HEIGHT_ADJUST = 0;
	
	private ArrayList<Rectangle> parts;
	private ArrayList<String> contents;
	private ArrayList<Integer> lineIndices;
	private Segment seg;
	
	public SegRegion(int x, int y) {
		super(new Rectangle(x, y, 0, 0));
	}

	public SegRegion(ArrayList<Rectangle> parts, ArrayList<String> contents,
			ArrayList<Integer> lineIndices, Segment seg) {
		super(parts.get(0));
		super.setW(parts.get(0).getW() + WIDTH_ADJUST);
		super.setH(parts.get(0).getH() + HEIGHT_ADJUST);
		
		for (Rectangle r : parts) {
			r.setW(r.getW() + WIDTH_ADJUST);
			r.setH(r.getH() + HEIGHT_ADJUST);
		}
		this.parts = parts;
		this.contents = contents;
		this.lineIndices = lineIndices;
		this.seg = seg;
	}

	@Override
	public int compareTo(Shape other) {
		if (this.getY() < other.getY()) return -1;
		if (this.getY() == other.getY()) {
			if (this.getX() < other.getX()) return -1;
			if (this.getX() == other.getX()) return 0;
			return 1;
		} else {
			return -1;
		}
	}
	
	public ArrayList<Rectangle> getParts() {
		return parts;
	}

	public Segment getSeg() {
		return seg;
	}

	public void setSeg(Segment seg) {
		this.seg = seg;
	}

	public void setLineIndices(ArrayList<Integer> lineIndices) {
		this.lineIndices = lineIndices;
	}

	public ArrayList<Integer> getLineIndices() {
		return lineIndices;
	}

	public void setContents(ArrayList<String> contents) {
		this.contents = contents;
	}

	public ArrayList<String> getContents() {
		return contents;
	}
}
