package titech.region;


public class CharRegion extends Region implements Comparable<CharRegion> {

	private int charIndex;
	private char c;

	public CharRegion(int charIndex) {
		super(-1, -1, -1, -1);
		this.charIndex = charIndex;
	}

	public CharRegion(int charIndex, char c, int tlx, int tly, int brx, int bry) {
		super(tlx, tly, brx, bry);
		this.charIndex = charIndex;
		this.c = c;
	}

	public int getCharIndex() {
		return charIndex;
	}

	public void setCharIndex(int charIndex) {
		this.charIndex = charIndex;
	}

	public char getC() {
		return c;
	}

	public void setC(char c) {
		this.c = c;
	}

	@Override
	public int compareTo(CharRegion another) {
		return charIndex == another.getCharIndex() ? 0 :
				charIndex > another.getCharIndex() ? 1 : -1;
	}
}
