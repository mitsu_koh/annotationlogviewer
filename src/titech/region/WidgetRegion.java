package titech.region;


public class WidgetRegion extends Region {

	private String widgetName;

	public WidgetRegion(String widgetName, int tlx, int tly, int brx, int bry) {
		super(tlx, tly, brx, bry);
		this.widgetName = widgetName;
	}

	public String getWidgetName() {
		return widgetName;
	}

	public void setWidgetName(String widgetName) {
		this.widgetName = widgetName;
	}
}
