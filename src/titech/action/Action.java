package titech.action;

public class Action implements Comparable<Action> {
	
	protected static long baseTime = 0;
	
	private long time;
	
	public Action(long time) {
		this.time = time;
	}
	
	public static void setBaseTime(long baseTime_) {
		baseTime = baseTime_;
	}
	
	public static long getBaseTime() {
		return baseTime;
	}
	
	public void setTime(long time) {
		this.time = time;
	}
	
	public long getTime() {
		return time;
	}
	
	public long getTimeInUTC() {
		try {
			if (baseTime == 0)
				throw new Exception("baseTime is not initialized.");
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(-1);
		}
		return baseTime + time;
	}
	
	public String toPrettyStr() {
		return null;
	}
	
	@Override
	public int compareTo(Action rhs) {
		if (this.getTime() == rhs.getTime()) return 0;
		if (this.getTime() > rhs.getTime()) return 1;
		return -1;
	}
}
