package titech.action;

import java.util.ArrayList;

import titech.annotation.Segment;
import titech.region.SegRegion;

public class Fixation extends Action {
	
	private long duration;
	private int x;
	private int y;
	private int adjX;
	private int adjY;
	private int npoints;
	private int lineIndex;
	
	private Integer charIndex = null;
	
	// These are not used if fixation isn't on segment.
	private SegRegion segRegion = null;
	private Integer partIndex = null;
	
	private int errPoints;
	
	public Fixation(long timestamp, long duration, int x, int y, int npoints) {
		super(timestamp);
		this.duration = duration;
		this.x = x;
		this.y = y;
		this.npoints = npoints;
		this.adjX = -1;
		this.adjY = -1;
		this.errPoints = 0;
	}
	
	public Fixation(long timestamp) {
		super(timestamp);
	}
	
	public Fixation(Fixation f) {
		super(f.getTime());
		this.duration = f.getDuration();
		this.x = f.getX();
		this.y = f.getY();
		this.npoints = f.getNpoints();
		this.adjX = f.getAdjX();
		this.adjY = f.getAdjY();
		this.lineIndex = f.getLineIndex();
		this.charIndex = f.getCharIndex();
		this.segRegion = f.getSegRegion();
		this.partIndex = f.getPartIndex();
		this.errPoints = f.getErrPoints();
	}
	
	public Fixation(final Fixation a, final Fixation b) {
		super(Math.min(a.getTime(), b.getTime()));
		final int n = a.getNpoints() + b.getNpoints();
		this.duration = Math.max(a.getEndTime(), b.getEndTime()) - Math.min(a.getTime(), b.getTime());
		this.x = (a.getX() * a.getNpoints() + b.getX() * b.getNpoints()) / n;
		this.y = (a.getY() * a.getNpoints() + b.getY() * b.getNpoints()) / n;
		this.npoints = n;
		this.errPoints = 0;
	}
	
	public boolean onSeg() {
		return (segRegion != null);
	}

	@Override
	public int compareTo(Action rhs) {
		if (rhs.getTime() < this.getTime()) return 1;
		if (rhs.getTime() < this.getEndTime()) return 0;
		return -1;
	}
	
	@Override
	public String toString() {
		return String.format("%.3f\t"+"%.3f\t"+"%5d\t"+"%5d\t"+"%3d\t"+"%.3f",
				getTime()/1000.0, (getTime()+duration)/1000.0, x, y, npoints, duration/1000.0);
	}
	
	@Override
	public String toPrettyStr() {
		return String.format("    (%04d, %04d) %.3f", x, y, duration/1000.0);
	}
	
	public String toFormatStr() {
		int segNameId = 0;
		String fmtStr = "*";
		if (onSeg()) {
			Segment seg = segRegion.getSeg();
			segNameId = (seg.getTagName() == Segment.PRED ? 1 : 2);
			fmtStr = seg.toFormatStr();
		}
		return String.format("%.3f,"+"%.3f,"+"%d,"+"%d,"+"%d,"+"%d,"+"%.3f\t" +
				"%d\t" +
				"%d\t" +
				"%s",
				getTimeInUTC()/1000.0, (getTimeInUTC()+duration)/1000.0,
				x, y, npoints, errPoints, duration/1000.0,
				segNameId,
				charIndex,
				fmtStr);
	}
	
	//--------------------------------------------------
	// Getters and Setters

	public long getDuration() {
		return duration;
	}

	public int getNpoints() {
		return npoints;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public void setX(int x) {
		this.x = x;
	}

	public void setY(int y) {
		this.y = y;
	}

	public void setDuration(long duration) {
		this.duration = duration;
	}

	public void setNpoints(int npoints) {
		this.npoints = npoints;
	}
	
	public long getEndTime() {
		return getTime() + duration;
	}

	public int getAdjX() {
		return adjX;
	}

	public int getAdjY() {
		return adjY;
	}

	public void setAdjX(int adjX) {
		this.adjX = adjX;
	}

	public void setAdjY(int adjY) {
		this.adjY = adjY;
	}

	public int getLineIndex() {
		return lineIndex;
	}

	public void setLineIndex(int lineIndex) {
		this.lineIndex = lineIndex;
	}

	public SegRegion getSegRegion() {
		return segRegion;
	}

	public void setSegRegion(SegRegion segRegion) {
		this.segRegion = segRegion;
	}

	public int getPartIndex() {
		return partIndex;
	}

	public void setPartIndex(Integer partIndex) {
		this.partIndex = partIndex;
	}

	public void setCharIndex(Integer charIndex) {
		this.charIndex = charIndex;
	}

	public int getCharIndex() {
		return this.charIndex;
	}

	public void setErrPoints(int errPoints) {
		this.errPoints = errPoints;
	}

	public int getErrPoints() {
		return errPoints;
	}
}
