package titech.action;

import titech.AppConfig;

public class Glance extends Action {

	private int x;
	private int y;
	private boolean error;

	public Glance(long ms) {
		super(ms);
	}

	public Glance(long timestamp, int x, int y) {
		super(timestamp);
		if (!AppConfig.checkPoint(x, y))
			x = y = -1;
		this.x = x;
		this.y = y;
		error = (this.x == -1) || (this.y == -1);
	}

	@Override
	public String toString() {
		return String.format("%.3f\t"+"%5d\t"+"%5d", getTime()/1000.0, x, y);
	}

	//--------------------------------------------------
	// Getters and Setters

	public int getX() {
		return x;
	}
	

	public int getY() {
		return y;
	}
	
	public boolean isError() {
		return error;
	}
}
