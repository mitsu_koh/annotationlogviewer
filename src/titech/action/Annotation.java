package titech.action;

import java.util.HashMap;

import titech.annotation.Link;
import titech.annotation.Segment;

import static titech.util.EasyGetter.*;

public class Annotation extends Action {

	public enum Kind {
		create_seg_start,
		create_seg_end,
		create_link_start,
		create_link_end,
		anchor_seg,
		create_link_from_anchor,
		create_tmp_link_start,
		create_tmp_link_end,
		select_seg,
		select_link,
		adjust_seg,
		delete_seg,
		delete_link,
		select_tagdef,
		annotation_start,
		annotation_end
	};
	
	private final static HashMap<Kind, Boolean> isAltering =
		new HashMap<Kind, Boolean>() {{
			put(Kind.create_seg_start, true);
			put(Kind.create_seg_end, true);
			// FIXME temporarily false for VideoViewer
			put(Kind.create_link_start, false);
			put(Kind.create_link_end, true);
			put(Kind.anchor_seg, false);
			put(Kind.create_link_from_anchor, true);
			put(Kind.create_tmp_link_start, false);
			put(Kind.create_tmp_link_end, false);
			put(Kind.select_seg, false);
			put(Kind.select_link, false);
			put(Kind.adjust_seg, false);
			put(Kind.delete_seg, true);
			put(Kind.delete_link, true);
			put(Kind.select_tagdef, false);
			put(Kind.annotation_start, false);
			put(Kind.annotation_end, false);
		}};

	public final static String BLANK_CHAR = "*";
	public final static int BLANK_TAG_INFO = Integer.MAX_VALUE;

	// Information of Annotation Action
	Kind kind;
	int tagId;
	String tagName;
	int tagInfo1;
	int tagInfo2;
	String tagContent;
	String actionInfo;

	public Annotation(long timestamp) {
		super(timestamp);
	}

	public Annotation(String timestamp, String actionName, String tagId,
			String tagName, String tagInfo1, String tagInfo2, String tagContent, String actionInfo) {

		super(Long.parseLong(timestamp));
		this.kind = Kind.valueOf(actionName);
		this.tagId = tagId.equals(BLANK_CHAR) ? BLANK_TAG_INFO : Integer.parseInt(tagId);
		this.tagName = tagName.equals(BLANK_CHAR) ? null : tagName;
		this.tagInfo1 = tagInfo1.equals(BLANK_CHAR) ? BLANK_TAG_INFO : Integer.parseInt(tagInfo1);
		this.tagInfo2 = tagInfo2.equals(BLANK_CHAR) ? BLANK_TAG_INFO : Integer.parseInt(tagInfo2);
		this.tagContent = tagContent.equals(BLANK_CHAR) ? null : tagContent;
		this.actionInfo = actionInfo.equals(BLANK_CHAR) ? null : actionInfo;
	}
	
	public boolean isAltering() {
		return isAltering.get(kind);
	}

	@Override
	public String toString() {
		String ret = "(";
		ret += String.format("%.3f ", getTimeInUTC() / 1000.0);
		ret += kind.name() + ", ";
		// ret += (tagId == BLANK_TAG_INFO ? "*" : tagId) + ",";
		ret += (tagName == null ? "*" : tagName) + ",";
		ret += "[" + (tagInfo1 == BLANK_TAG_INFO ? "*" :
			getTagManager().get(tagInfo1).toString()) + "]->";
		ret += "[" + (tagInfo2 == BLANK_TAG_INFO ? "*" :
			getTagManager().get(tagInfo2).toString()) + "],";
		// ret += (tagContent == null ? "*" : tagContent) + ",";
		// ret += (actionInfo == null ? "*" : actionInfo);
		ret += ")";
		return ret;
	}
	
	// FIXME This is implemented only for the predicate-argument task in 2012.
	public String toPrettyStr() {
		String ret = "";
		try {
			switch (kind) {
			case create_link_start:
			case create_link_end:
				ret += "+ ";
				ret += "(" + getTagManager().get(tagInfo1).getContent() + ")";
				ret += "->";
				ret += "(" + getTagManager().get(tagInfo2).getContent() + ")";
				break;
			case create_tmp_link_start:
			case create_tmp_link_end:
				ret += "/ ";
				break;
			case delete_link:
				ret += "- ";
				ret += "(" + getTagManager().get(tagInfo1).getContent() + ")";
				ret += "->";
				ret += "(" + getTagManager().get(tagInfo2).getContent() + ")";
				break;
			case select_link:
				ret += "@ ";
				ret += "(" + getTagManager().get(tagInfo1).getContent() + ")";
				ret += "->";
				ret += "(" + getTagManager().get(tagInfo2).getContent() + ")";
				break;
			case select_seg:
				ret += "Select seg.";
				break;
			case select_tagdef:
				ret += "* ";
				ret += "[" + Link.toPrettyName(tagName) + "]";
				break;
			case annotation_start:
				ret += "# ";
				ret += "Annotation starts.";
				break;
			case annotation_end:
				ret += "# ";
				ret += "Annotation ends.";
				break;
			default:
				throw new Exception("Kind error (" + kind.name() + ").");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ret;
	}
	
	public String toFormatStr() {
		String ret = "";
		
		ret += String.format("%.3f" + "\t", getTimeInUTC() / 1000.0);
		ret += kind.name() + "\t";
		
		try {
			switch (kind) {
			case create_link_start:
			case create_link_end:
				Segment pred = getTagManager().get(tagInfo2);
				Segment arg = getTagManager().get(tagInfo1);
				ret += tagName + "_";
				ret += pred.toFormatStr() + "\t";
				ret += arg.toFormatStr();
				break;
			case create_tmp_link_start:
			case create_tmp_link_end:
				ret += "/ ";
				break;
			case delete_link:
				ret += "- ";
				ret += "(" + getTagManager().get(tagInfo1).getContent() + ")";
				ret += "->";
				ret += "(" + getTagManager().get(tagInfo2).getContent() + ")";
				break;
			case select_link:
				ret += "@ ";
				ret += "(" + getTagManager().get(tagInfo1).getContent() + ")";
				ret += "->";
				ret += "(" + getTagManager().get(tagInfo2).getContent() + ")";
				break;
			case select_seg:
				ret += "Select seg.";
				break;
			case select_tagdef:
				ret += "* ";
				ret += "[" + Link.toPrettyName(tagName) + "]";
				break;
			case annotation_start:
				ret += "# ";
				ret += "Annotation starts.";
				break;
			case annotation_end:
				ret += "# ";
				ret += "Annotation ends.";
				break;
			default:
				throw new Exception("Kind error (" + kind.name() + ".");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ret;
	}

	//--------------------------------------------------
	// Getters and Setters

	public Kind getKind() {
		return kind;
	}

	public int getTagId() {
		return tagId;
	}

	public String getTagName() {
		return tagName;
	}

	public int getTagInfo1() {
		return tagInfo1;
	}

	public int getTagInfo2() {
		return tagInfo2;
	}

	public String getTagContent() {
		return tagContent;
	}

	public String getActionInfo() {
		return actionInfo;
	}

	public void setKind(Kind kind) {
		this.kind = kind;
	}

	public void setTagId(int tagId) {
		this.tagId = tagId;
	}

	public void setTagName(String tagName) {
		this.tagName = tagName;
	}

	public void setTagInfo1(int tagInfo1) {
		this.tagInfo1 = tagInfo1;
	}

	public void setTagInfo2(int tagInfo2) {
		this.tagInfo2 = tagInfo2;
	}

	public void setTagContent(String tagContent) {
		this.tagContent = tagContent;
	}

	public void setActionInfo(String actionInfo) {
		this.actionInfo = actionInfo;
	}
}
