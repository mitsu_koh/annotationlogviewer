package titech;

import java.io.File;
import java.io.IOException;

import static titech.util.EasyGetter.*;

public class ImageViewer {
	
	private static boolean noWindowFlag = false;

	public static void main(String[] args) {
		try {
			String opts = null;
			String logDirPath = null;
			
			switch (args.length) {
			case 1:
				logDirPath = args[0];
				break;
			case 2:
				opts = args[0];
				logDirPath = args[1];
				if (!opts.startsWith("-"))
					throw new Exception("Error opts");
				if (opts.indexOf("s") != -1)
					noWindowFlag = true;
				break;
			default:
				throw new Exception("Error args");
			}
			
			ImageViewer app = new ImageViewer(logDirPath);
			if (!noWindowFlag) app.run();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public ImageViewer(String logDirPath) {
		getAppManager().initialize(logDirPath, true);
	}
	
	public void run() {
		getAppManager().initShapeManager();
		getUiManager().initialize(getAppManager().loadSnaps());
		getUiManager().run();
	}
}
