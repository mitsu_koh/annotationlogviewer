package titech.shape;

import titech.ui.DrawableTarget;

public class Line extends Shape {

	private int xx;
	private int yy;
	
	public Line(int ax, int ay, int bx, int by) {
		super(ax, ay);
		this.xx = bx;
		this.yy = by;
	}

	public Line(int ax, int ay, int bx, int by, ShapeAttribute attr) {
		super(ax, ay, attr);
		this.xx = bx;
		this.yy = by;
	}

	public int getAx() {
		return x;
	}

	public int getAy() {
		return y;
	}

	public int getBx() {
		return xx;
	}

	public int getBy() {
		return yy;
	}

	@Override
	public void draw(DrawableTarget target) {
		target.draw(this);
	}
}
