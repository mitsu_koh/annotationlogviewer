package titech.shape;

import java.awt.Color;

public class ShapeAttribute {

	private Color color;
	private boolean isFilled;

	public ShapeAttribute(Color color, boolean isFilled) {
		this.color = color;
		this.isFilled = isFilled;
	}

	public Color getColor() {
		return color;
	}

	public boolean isFilled() {
		return isFilled;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	public void setFilled(boolean isFilled) {
		this.isFilled = isFilled;
	}
}
