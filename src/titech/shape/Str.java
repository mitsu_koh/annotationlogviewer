package titech.shape;

import titech.ui.DrawableTarget;

public class Str extends Shape {

	String s;

	public Str(String s, int x, int y) {
		super(x, y);
		this.s = s;
	}

	public String getS() {
		return s;
	}

	public void setS(String s) {
		this.s = s;
	}

	@Override
	public void draw(DrawableTarget target) {
		target.draw(this);
	}
}
