package titech.shape;

import titech.ui.DrawableTarget;

public abstract class Shape {

	protected int x;
	protected int y;
	protected ShapeAttribute attr;

	protected Shape() {
		x = 0;
		y = 0;
	}

	protected Shape(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	protected Shape(int x, int y, ShapeAttribute attr) {
		this.x = x;
		this.y = y;
		this.attr = attr;
	}

	public abstract void draw(DrawableTarget target);

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public ShapeAttribute getAttr() {
		return attr;
	}

	public void setAttr(ShapeAttribute attr) {
		this.attr = attr;
	}

}