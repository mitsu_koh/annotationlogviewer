package titech.shape;

import titech.ui.DrawableTarget;

public class Rectangle extends Shape {

	private int w;
	private int h;

	public Rectangle(int x, int y, int w, int h) {
		super(x, y);
		this.w = w;
		this.h = h;
	}

	public Rectangle(int x, int y, int w, int h, ShapeAttribute attr) {
		super(x, y, attr);
		this.w = w;
		this.h = h;
	}
	
	public Rectangle(Rectangle r) {
		super(r.getX(), r.getY(), r.getAttr());
		this.w = r.getW();
		this.h = r.getH();
	}
	
	public int getMidX() {
		return x + w / 2;
	}
	
	public int getMidY() {
		return y + h / 2;
	}

	@Override
	public void draw(DrawableTarget target) {
		target.draw(this);
	}

	public int getW() {
		return w;
	}

	public void setW(int w) {
		this.w = w;
	}

	public int getH() {
		return h;
	}

	public void setH(int h) {
		this.h = h;
	}

	@Override
	public String toString() {
		return x + "," + y + "," + w + "," + h;
	}
}
