package titech.shape;

import titech.ui.DrawableTarget;

public class Circle extends Shape {

	private int r;

	public Circle(int x, int y, int r, ShapeAttribute attr) {
		super(x, y, attr);
		this.r = r;
	}

	@Override
	public void draw(DrawableTarget target) {
		target.draw(this);
	}

	public int getR() {
		return r;
	}

	public void setR(int r) {
		this.r = r;
	}
}
