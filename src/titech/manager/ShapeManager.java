package titech.manager;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Map.Entry;
import java.util.TreeMap;

import titech.action.Annotation;
import titech.action.Glance;
import titech.action.Fixation;
import titech.annotation.Segment;
import titech.region.Region;
import titech.region.SegRegion;
import titech.shape.*;
import titech.ui.DrawableTarget;
import titech.util.ExtArrayList;
import titech.util.HTMLColor;
import static titech.util.EasyGetter.*;

public class ShapeManager {
	
	private static final Color NOW_FIXATION_COLOR = HTMLColor.get("red");
	private static final Color NOW_SACCADE_COLOR = NOW_FIXATION_COLOR;
	private static final Color OLD_FIXATIONS_COLOR = HTMLColor.get("blue");
	private static final Color OLD_SACCADES_COLOR = OLD_FIXATIONS_COLOR;
	
	private final ShapeAttribute PRED_ATTR = new ShapeAttribute(HTMLColor.get("black"), true);
	private final ShapeAttribute NOUN_ATTR = new ShapeAttribute(HTMLColor.get("black"), true);
	private final ShapeAttribute NEXT_PRED_ATTR = new ShapeAttribute(HTMLColor.get("darkblue"), false);
	private final ShapeAttribute NEXT_NOUN_ATTR = new ShapeAttribute(HTMLColor.get("dimgray"), false);
	private final ShapeAttribute LINE_ATTR = new ShapeAttribute(HTMLColor.get("black"), false);
	
	private static final double FIXATION_CIRCLE_SIZE_MULT = 0.05;

	private static final ShapeManager instance = new ShapeManager();

	private ArrayList<Shape> shapes;
	private boolean drawing;
	private boolean adjusting;
	private int fixationNum;

	private ArrayList<Annotation> actions;
	private ArrayList<Glance> trackings;
	private ArrayList<Fixation> fixations;

	public ShapeManager() {
		drawing = true;
		adjusting = true;
		fixationNum = 5;
	}

	public static ShapeManager instance() {
		return instance;
	}
	
	public ShapeManager getSegDrawer() {
		shapes = new ArrayList<Shape>();
		for (SegRegion sr : getRegionManager().getSegRegions()) {
			ShapeAttribute attr = (sr.getSeg().getTagName() == Segment.PRED ?
					PRED_ATTR : NOUN_ATTR);
			for (Rectangle r : sr.getParts()) {
				r.setAttr(attr);
				shapes.add(r);
			}
		}
		return instance;
	}
	
	public ShapeManager getNextSegDrawer(int time) {
		shapes = new ArrayList<Shape>();
		Annotation next = getActionManager().getNextAltAnno(time);
		int si = next.getTagInfo1();
		int di = next.getTagInfo2();
		SegRegion sr = getRegionManager().getSegRegion(si);
		SegRegion dr = getRegionManager().getSegRegion(di);
		
		if (sr == null || dr == null)
			return instance;
		
		for (Rectangle r : sr.getParts()) {
			r.setAttr(sr.getSeg().getTagName() == Segment.PRED ?
					NEXT_PRED_ATTR : NEXT_NOUN_ATTR);
			shapes.add(r);
		}
		for (Rectangle r : dr.getParts()) {
			r.setAttr(dr.getSeg().getTagName() == Segment.PRED ?
					NEXT_PRED_ATTR : NEXT_NOUN_ATTR);
			shapes.add(r);
		}
		return instance;
	}


	public ShapeManager getBorderDrawer() {
		shapes = new ArrayList<Shape>();
		for (Shape s : getRegionManager().getBorderRegions()) {
			s.setAttr(LINE_ATTR);
			shapes.add(s);
		}
		return instance;
	}

	public ShapeManager getLineDrawer(int fromMs, int toMs) {
		shapes = new ArrayList<Shape>();
		for (Rectangle r : getRegionManager().getLineRegions())
			shapes.add(r);
		return instance();
	}
	
	public ShapeManager getFixationDrawer(int toMs) {
		shapes = new ArrayList<Shape>();
		final int index[] = ActionManager.instance().getFixationSection(toMs);
		int from = index[0];
		final int to = index[1];
		
		final boolean hasFixationNow = (from != to);
		
		from = Math.max(0, Math.min(from, to-fixationNum));
		
		if (drawing) {
			for (int i = from+1; i < to; ++i) {
				ShapeAttribute attr = new ShapeAttribute(
						((i == to-1 && hasFixationNow) ? NOW_SACCADE_COLOR : OLD_SACCADES_COLOR), false);
				Fixation a = fixations.get(i-1);
				Fixation b = fixations.get(i);
				
				Line l = (adjusting ?
						new Line(a.getAdjX(), a.getAdjY(), b.getAdjX(), b.getAdjY(), attr) :
						new Line(a.getX(), a.getY(), b.getX(), b.getY(), attr));
				shapes.add(l);
			}
		}
		for (int i = from; i < to; ++i) {
			ShapeAttribute attr = new ShapeAttribute(
					((i == to-1 && hasFixationNow) ? NOW_FIXATION_COLOR : OLD_FIXATIONS_COLOR), true);
			Fixation a = fixations.get(i);
			
			int size = (int)(a.getDuration() * FIXATION_CIRCLE_SIZE_MULT);
			Circle c = (adjusting ?
					new Circle(a.getAdjX(), a.getAdjY(), size, attr) :
					new Circle(a.getX(), a.getY(), size, attr));
			shapes.add(c);
		}
		return instance;
	}

	public ShapeManager getAnnotationDrawer(int fromMs, int toMs) {
		shapes = new ArrayList<Shape>();
		TreeMap<Integer, ArrayList<Shape>> shapes_ = new TreeMap<Integer, ArrayList<Shape>>();
		final int from = ActionManager.instance().getActionIndex(fromMs);
		final int to = ActionManager.instance().getActionIndex(toMs);

		for (int i = from; i <= to; ++i) {
			Annotation aa = actions.get(i);
			if (i == to) {
				UiManager.instance().setActionLabel(aa);
			}
			switch (aa.getKind()) {
				case create_seg_start:
					break;
				case create_seg_end:
					ArrayList<Shape> regions = RegionManager.instance().getSegmentRegion(aa);
					shapes_.put(aa.getTagId(), regions);
					break;
				case create_link_start:
					break;
				case create_link_end:
					break;
				case anchor_seg:
					break;
				case create_link_from_anchor:
					break;
				case create_tmp_link_start:
					break;
				case create_tmp_link_end:
					break;
				case select_seg:
					break;
				case select_link:
					break;
				case adjust_seg:
					ArrayList<Shape> new_regions = RegionManager.instance().getSegmentRegion(aa);
					shapes_.remove(aa.getTagId());
					shapes_.put(aa.getTagId(), new_regions);
					break;
				case delete_seg:
					shapes_.remove(aa.getTagId());
					break;
				case delete_link:
					break;
				case select_tagdef:
					if (i == to) {
						Region r = RegionManager.instance().getWidgetRegions().get(2);
						ArrayList<Shape> a = new ArrayList<Shape>();
						a.add(new Rectangle(r.getX(), r.getY(), r.getW(), r.getH()));
						shapes_.put(Integer.MAX_VALUE, a);
					}
					break;
				case annotation_start:
					break;
				case annotation_end:
					break;
				default:
					break;
			}
		}

		for (Entry<Integer, ArrayList<Shape>> e : shapes_.entrySet())
			for (Shape s : e.getValue())
				shapes.add(s);

		return instance;
	}

	public void draw(DrawableTarget target) {
		for (Shape s : shapes)
			s.draw(target);
	}
	
	//--------------------------------------------------
	// Getters and Setters

	public ArrayList<Shape> getShapes() {
		return shapes;
	}
	
	public void setShapes(ArrayList<Shape> shapes) {
		this.shapes = shapes;
	}

	public ArrayList<Annotation> getActions() {
		return actions;
	}

	public void setActions(ArrayList<Annotation> actions) {
		this.actions = actions;
	}

	public ArrayList<Glance> getTrackings() {
		return trackings;
	}

	public void setTrackings(ArrayList<Glance> trackings) {
		this.trackings = trackings;
	}

	public boolean isDrawing() {
		return drawing;
	}
	
	public boolean isAdjusting() {
		return adjusting;
	}

	public void setDrawing(boolean drawFlag) {
		this.drawing = drawFlag;
	}

	public void toggleDrawing() {
		drawing = drawing ? false : true;
	}
	
	public void toggleAdjusting() {
		adjusting = adjusting ? false : true;
	}

	public ArrayList<Fixation> getFixations() {
		return fixations;
	}

	public void setFixations(ArrayList<Fixation> fixations) {
		this.fixations = fixations;
	}

	public int getFixationNum() {
		return fixationNum;
	}

	public void setFixationNum(int fixationNum) {
		this.fixationNum = fixationNum;
	}
}
