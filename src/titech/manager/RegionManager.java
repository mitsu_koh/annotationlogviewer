package titech.manager;

import java.util.ArrayList;
import java.util.TreeMap;
import java.util.TreeSet;

import titech.AppConfig;
import titech.action.Annotation;
import titech.action.Fixation;
import titech.annotation.Segment;
import titech.region.CharRegion;
import titech.region.SegRegion;
import titech.region.WidgetRegion;
import titech.shape.Line;
import titech.shape.Rectangle;
import titech.shape.Shape;
import titech.util.ExtArrayList;
import static titech.util.EasyGetter.*;

public class RegionManager {
	
	private final static RegionManager instance = new RegionManager();

	private TreeSet<CharRegion> charRegions;
	private ArrayList<WidgetRegion> widgetRegions;
	private ArrayList<Rectangle> lineRegions;
	private ArrayList<Shape> borderRegions;
	private ExtArrayList<SegRegion> segRegions;
	private ExtArrayList<Integer> lineStarts;
	private TreeMap<Integer, SegRegion> segMap;

	public RegionManager() {
		lineRegions = new ArrayList<Rectangle>();
		borderRegions = new ArrayList<Shape>();
		segRegions = new ExtArrayList<SegRegion>();
		segMap = new TreeMap<Integer, SegRegion>();
	}

	public static RegionManager instance() {
		return instance;
	}

	public ArrayList<Shape> getSegmentRegion(Annotation aa) {
		ArrayList<Shape> shapes = new ArrayList<Shape>();
		final int start = aa.getTagInfo1();
		final int end = aa.getTagInfo2();
		for (int i = start; i < end; ++i) {
			CharRegion cr = charRegions.floor(new CharRegion(i));
			shapes.add(new Rectangle(cr.getX(), cr.getY(), cr.getW(), cr.getH()));
		}
		return shapes;
	}
	
	// Do NOT call before calcLineRegion() because of matchLine().
	public void calcSegRegion() {
		boolean isLast = false;
		for (Segment seg : getTagManager().get()) {
			
			CharRegion st = charRegions.floor(new CharRegion(seg.getStartIndex()));
			CharRegion end = charRegions.floor(new CharRegion(seg.getEndIndex()));
			
			ArrayList<Rectangle> parts = new ArrayList<Rectangle>();
			ArrayList<String> contents = new ArrayList<String>();
			ArrayList<Integer> lines = new ArrayList<Integer>();
			
			int x = -1;
			int y = -1;
			int w = -1;
			int h = st.getH();
			int i = 0;
			int cnt = 0;
			int line = matchLine(st.getY());
			CharRegion prev = null;
			
			for (CharRegion cr : charRegions.subSet(st, end)) {
				if (cr.getY() >= AppConfig.TOBII_HEIGHT_PX) { isLast = true; break; }
				
				if (y == -1) {
					x = cr.getX();
					y = cr.getY();
				} else if (cr.getY() != y) {
					w = prev.getX() + prev.getW() - x;
					parts.add(new Rectangle(x, y, w, h));
					contents.add(seg.getContent().substring(i-cnt, i));
					lines.add(line);
					x = cr.getX();
					y = cr.getY();
					cnt = 0;
					++line;
				}
				prev = cr;
				++i;
				++cnt;
				if (seg.getStartIndex()+i == seg.getEndIndex()) break;
			}
			if (isLast) break;
			
			w = prev.getX() + prev.getW() - x;
			parts.add(new Rectangle(x, y, w, h));
			contents.add(seg.getContent().substring(i-cnt, i));
			lines.add(line);
//			for (int j = 0; j < parts.size(); ++j)
//				System.out.println(parts.get(j) + " " + contents.get(j) + " " + lines.get(j));
			SegRegion sr = new SegRegion(parts, contents, lines, seg);
			
//			System.out.print(seg.toFormatStr() + "\t");
//			for (Rectangle r : parts)
//				System.out.print(r + "\t");
//			System.out.println();
			
			segRegions.add(sr);
			segMap.put(seg.getId(), sr);
		}
	}

	public void calcLineRegion() {
		ArrayList<Integer> top_ys = new ArrayList<Integer>();
		ArrayList<Integer> btm_ys = new ArrayList<Integer>();
		
		AppConfig.fontSize = Math.abs(
			charRegions.first().getX() -
			charRegions.higher(charRegions.first()).getX());

		int maxRaw = 0, tmp = 0;
		for (CharRegion cr : charRegions) {
			if (top_ys.size() == 0) {
				top_ys.add(cr.getY());
				btm_ys.add(cr.getY() + cr.getH());
			} else if (top_ys.get(top_ys.size()-1) != cr.getY()) {
				if (cr.getY() >= AppConfig.TOBII_HEIGHT_PX) break;
				top_ys.add(cr.getY());
				btm_ys.add(cr.getY() + cr.getH());
				maxRaw = Math.max(maxRaw, tmp);
				tmp = 0;
			}
			++tmp;
		}
		int maxCol = 0;
		for (int i = 0; i < top_ys.size(); ++i) {
			if ((top_ys.get(i) + btm_ys.get(i)) / 2 > AppConfig.TOBII_HEIGHT_PX) break;
			++maxCol;
		}

		// Border Region
		for (int i = 1; i < top_ys.size(); ++i) {
			int mid_y = (top_ys.get(i) + btm_ys.get(i-1)) / 2;
			Line l = new Line(0, mid_y, AppConfig.TOBII_WIDTH_PX, mid_y);
			borderRegions.add(l);
		}

		// Line Region
		int x = 0;
		int y = 0;
		int h = borderRegions.get(0).getY() - 0;
		int w = AppConfig.TOBII_WIDTH_PX;
		Rectangle r = new Rectangle(x, y, w, h);
		lineRegions.add(r);
		for (int i = 1; i < borderRegions.size(); ++i) {
			y = borderRegions.get(i-1).getY();
			h = borderRegions.get(i).getY() - y;
			r = new Rectangle(x, y, w, h);
			lineRegions.add(r);
		}
		y = borderRegions.get(borderRegions.size()-1).getY();
		h = AppConfig.TOBII_HEIGHT_PX - y;
		r = new Rectangle(x, y, w, h);
		lineRegions.add(r);
	}
	
	public void adjustFixations(ExtArrayList<Fixation> fixations) {
		for (Fixation f : fixations) {
			// vertical
			matchLine(f);
			
			// horizontal (PRED = NOUN adjustment)
			matchSeg(f, AppConfig.fontSize);
		}
	}
	
	private int matchChar(int x, int y) {
		CharRegion best = null;
		int bestDist2 = Integer.MAX_VALUE;
		for (CharRegion cr : charRegions) {
			int dx = cr.getMidX() - x;
			int dy = cr.getMidY() - y;
			int dist2 = dx*dx + dy*dy;
			if (dist2 < bestDist2) {
				best = cr;
				bestDist2 = dist2;
			}
		}
		return best.getCharIndex();
	}
	
	private int matchLine(int y) {
		int i;
		for (i = 0; i < lineRegions.size(); ++i)
			if (y < lineRegions.get(i).getY()) break;
		return i-1;
	}
	
	private void matchLine(Fixation f) {
		int lineIndex = getRegionManager().getLineStarts().lowerBoundDecl(f.getY());
		int y = lineRegions.get(lineIndex).getMidY();
		f.setAdjY(y);
		f.setLineIndex(lineIndex);
		
		// re-match fixation if it is on blank line.
		int charIndex = matchChar(f.getX(), f.getAdjY());
		int yy = charRegions.floor(new CharRegion(charIndex)).getMidY();
		int checkLineIndex = matchLine(yy);
		if (lineIndex != checkLineIndex) {
			int prevDist = 10000;
			int nextDist = 10000;
			if (lineIndex - 1 >= 0)
				prevDist = f.getY() - getRegionManager().getLineStarts().get(lineIndex);
			if (lineIndex + 1 < lineRegions.size())
				nextDist = getRegionManager().getLineStarts().get(lineIndex + 1) - f.getY();
			int fixIndexTo = prevDist <= nextDist ? lineIndex - 1 : lineIndex + 1;
			int fixYTo = lineRegions.get(fixIndexTo).getMidY();
			f.setLineIndex(fixIndexTo);
			f.setAdjY(fixYTo);
//			System.out.println(f.getTime() + "\t" + lineIndex + "\t" + fixIndexTo);
		}
	}
	
	// Do NOT call before matchLine because of f.getAdjY().
	private boolean matchSeg(Fixation f, int margin) {
		ArrayList<SegRegion> cands = new ArrayList<SegRegion>();
		ArrayList<Integer> indices = new ArrayList<Integer>();
		
		for (SegRegion sr : segRegions) {
			for (int i = 0; i < sr.getParts().size(); ++i) {
				Rectangle r = sr.getParts().get(i);
				int ax = r.getX() - margin;
				int bx = r.getX() + r.getW() + margin;
				if (matchLine(r.getMidY()) != matchLine(f.getAdjY())) continue;
				if (ax <= f.getX() && f.getX() <= bx) {
					cands.add(sr);
					indices.add(i);
				}
			}
		}
		
		if (cands.size() == 0) {
			// Adjust to best char.
			int charIndex = matchChar(f.getX(), f.getAdjY());
			CharRegion cr = charRegions.floor(new CharRegion(charIndex));
			f.setAdjX(cr.getMidX());
			f.setAdjY(cr.getMidY());
			f.setCharIndex(charIndex);
			f.setSegRegion(null);
			f.setPartIndex(null);
			return false;
			
		} else {
			// Adjust to best segment, because of margin's over wrap.
			int bestDx = 10000;
			int best = -1;
			for (int i = 0; i < cands.size(); ++i) {
				Rectangle r = cands.get(i).getParts().get(indices.get(i));
				int dx = Math.min(
						Math.abs(r.getX() - f.getX()),
						Math.abs(r.getX() + r.getW() - f.getX()));
				if (dx < bestDx) {
					bestDx = dx;
					best = i;
				}
			}
			SegRegion sr = cands.get(best);
			int i = indices.get(best);
			Rectangle r = sr.getParts().get(i);
			f.setAdjX(r.getMidX());
			f.setAdjY(r.getMidY());
			f.setCharIndex(matchChar(f.getX(), f.getAdjY()));
			f.setSegRegion(sr);
			f.setPartIndex(i);
			return true;
		}
	}
	
	public SegRegion getSegRegion(int id) {
		return segMap.get(id);
	}

	//--------------------------------------------------
	// Getters and Setters

	public TreeSet<CharRegion> getCharRegions() {
		return charRegions;
	}

	public ArrayList<WidgetRegion> getWidgetRegions() {
		return widgetRegions;
	}

	public void setCharRegions(TreeSet<CharRegion> charRegions) {
		this.charRegions = charRegions;
	}

	public void setWidgetRegions(ArrayList<WidgetRegion> widgetRegions) {
		this.widgetRegions = widgetRegions;
	}

	public ArrayList<Rectangle> getLineRegions() {
		return lineRegions;
	}

	public void setLineRegions(ArrayList<Rectangle> lineRegions) {
		this.lineRegions = lineRegions;
	}

	public ArrayList<Shape> getBorderRegions() {
		return borderRegions;
	}

	public void setBorderRegions(ArrayList<Shape> borderRegions) {
		this.borderRegions = borderRegions;
	}

	public ExtArrayList<SegRegion> getSegRegions() {
		return segRegions;
	}

	public void setSegRegions(ExtArrayList<SegRegion> segRegions) {
		this.segRegions = segRegions;
	}

	public ExtArrayList<Integer> getLineStarts() {
		return lineStarts;
	}

	public void setLineStarts(ArrayList<Integer> lineStarts) {
		this.lineStarts = new ExtArrayList<Integer>(lineStarts);
	}
}
