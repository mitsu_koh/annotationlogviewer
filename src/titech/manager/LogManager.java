package titech.manager;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import titech.AppConfig;
import titech.action.Fixation;
import static titech.util.EasyGetter.*;

public class LogManager {
	
	private static final LogManager instance = new LogManager();
	
	public LogManager() {
	}
	
	public static LogManager instance() {
		return instance;
	}
	
	public void saveFixesList(ArrayList<String> body) {
		try {
			PrintWriter pw = new PrintWriter(new FileOutputStream(AppConfig.FIX_LIST_LOG_PATH));
			for (int i = 0; i < body.size(); ++i)
				pw.println(body.get(i));
			pw.flush();
			pw.close();
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(-1);
		}
	}
	
	public void saveFixations(ArrayList<Fixation> fixations) {
		try {
			PrintWriter pw = new PrintWriter(new FileOutputStream(AppConfig.FIXATION_LOG_PATH));
			for (int i = 0; i < fixations.size(); ++i)
				pw.println(fixations.get(i).toFormatStr());
			pw.flush();
			pw.close();
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(-1);
		}
	}
	
	public void saveWorkTime(ArrayList<String> body) {
		try {
			PrintWriter pw = new PrintWriter(new FileOutputStream(AppConfig.WORK_TIME_LOG_PATH));
			for (int i = 0; i < body.size(); ++i)
				pw.println(body.get(i));
			pw.flush();
			pw.close();
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(-1);
		}
	}
}
