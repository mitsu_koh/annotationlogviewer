package titech.manager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;
import java.util.TreeMap;

import titech.annotation.Link;
import titech.annotation.Segment;

public class TagManager {
	
	private static final TagManager instance = new TagManager();
	
	private TreeMap<Integer, Segment> segments;
	private TreeMap<Integer, Link> links;
	
	public TagManager() {
		segments = new TreeMap<Integer, Segment>();
		links = new TreeMap<Integer, Link>();
	}
	
	public static TagManager instance() {
		return instance;
	}

	public void setSegments(ArrayList<Segment> segments) {
		for (Segment s : segments)
			this.segments.put(s.getId(), s);
	}
	
	public void setLinks(TreeMap<Integer, Link> links) {
		this.links = links;
	}
	
	public boolean hasLink(int id) {
		return links.containsKey(id);
	}
	
	public Link getLink(int id) {
		if (hasLink(id)) {
			return links.get(id);
		} else {
			return null;
		}
	}
	
	public ArrayList<Link> getLinks() {
		ArrayList<Link> ret = new ArrayList<Link>();
		for (Map.Entry<Integer, Link> e : links.entrySet())
			ret.add(e.getValue());
		Collections.reverse(ret);
		return ret;
	}
	
	public Segment get(int id) {
		return segments.get(id);
	}
	
	public ArrayList<Segment> get() {
		ArrayList<Segment> ret = new ArrayList<Segment>();
		for (Map.Entry<Integer, Segment> e : segments.entrySet())
			ret.add(e.getValue());
		return ret;
	}
}
