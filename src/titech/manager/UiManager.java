package titech.manager;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Timer;

import javax.swing.JFrame;
import javax.swing.JSlider;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.w3c.dom.events.EventException;

import titech.action.Annotation;
import titech.ui.AnnoPanel;
import titech.ui.InfoPanel;
import titech.ui.MonitorPanel;
import titech.ui.TimePanel;
import titech.ui.UtilPanel;
import static titech.util.EasyGetter.*;

public class UiManager implements
		ChangeListener, ActionListener, ItemListener {
	
	private final int MONITOR_PANEL_WIDTH = 1280;
	private final int MONITOR_PANEL_HEIGHT = 1024;

	private static final UiManager instance = new UiManager();

	private JFrame mainFrame;
	private Container mainContentPane;
	private MonitorPanel monitorPanel;
	private TimePanel timePanel;
	private UtilPanel utilPanel;
	private InfoPanel infoPanel;
	
	private int toMs;
	private int fromMs;

	public UiManager() {
	}

	public final static UiManager instance() {
		return instance;
	}

	public void initialize(ArrayList<File> snaps) {
		mainFrame = new JFrame("AnnotationLogViewer");
		mainContentPane = mainFrame.getContentPane();

		monitorPanel = new MonitorPanel(
				MONITOR_PANEL_WIDTH, MONITOR_PANEL_HEIGHT, snaps);
		timePanel = new TimePanel(
				(int)(ActionManager.instance().getStartMs()),
				(int)(ActionManager.instance().getEndMs()));
		utilPanel = new UtilPanel();
		infoPanel = new InfoPanel(timePanel, MONITOR_PANEL_HEIGHT);

		mainContentPane.setLayout(new BorderLayout());
		mainContentPane.add(infoPanel, BorderLayout.WEST);
		mainContentPane.add(utilPanel, BorderLayout.EAST);
		mainContentPane.add(monitorPanel, BorderLayout.CENTER);
		mainContentPane.add(timePanel, BorderLayout.SOUTH);

		mainFrame.addComponentListener(new ComponentAdapter() {
			@Override
			public void componentResized(ComponentEvent e) {
				reflesh(fromMs, toMs, true);
				infoPanel.adjustSize();
			}
		});
		
		mainFrame.pack();
		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		fromMs = 0;
		toMs = 0;
	}
	
	public void run() {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				mainFrame.setVisible(true);
			}
		});
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		try {
			String command = e.getActionCommand();
			if (command.equals(UtilPanel.DRAW)) {
				ShapeManager.instance().toggleDrawing();
				reflesh();
			} else if (command.equals(UtilPanel.ADJUST)) {
				ShapeManager.instance().toggleAdjusting();
				reflesh();
			} else if (command.equals(UtilPanel.PLAY)) {
				timePanel.startTimer();
				utilPanel.setPlayEnabled(false);
				utilPanel.setStopEnabled(true);
			} else if (command.equals(UtilPanel.STOP)) {
				timePanel.stopTimer();
				utilPanel.setPlayEnabled(true);
				utilPanel.setStopEnabled(false);
			} else if (command.equals(UtilPanel.APPLY)) {
				ShapeManager.instance().setFixationNum(infoPanel.getFixationNum());
				timePanel.setPlaySpeed(infoPanel.getPlaySpeed());
			} else if (command.equals(TimePanel.TIMER)) {
				if (timePanel.getMsTime() < timePanel.getMaxMsTimestamp()) {
					int add = Math.min(timePanel.getTimerDelayMs(),
							timePanel.getMaxMsTimestamp() - timePanel.getMsTime());
					timePanel.addMsTime(add);
					infoPanel.renew();
				} else {
					timePanel.stopTimer();
					utilPanel.setPlayEnabled(true);
					utilPanel.setStopEnabled(false);
				}
			} else {
				throw new IOException("Unknown event " + e.getActionCommand());
			}
		} catch (Exception exception) {
			exception.printStackTrace();
			System.exit(-1);
		}
	}

	@Override
	public void stateChanged(ChangeEvent e) {
		final int newMsTime = ((JSlider)e.getSource()).getValue();
		setMsTime(toMs, newMsTime);
	}

	@Override
	public void itemStateChanged(ItemEvent e) {
	}

	private void setMsTime(final int fromMs, final int newMsTime) {
		timePanel.setMsTime(newMsTime);
		infoPanel.renew();
		reflesh(fromMs, newMsTime, true);
	}
	
	private void reflesh() {
		reflesh(fromMs, toMs, true);
	}

	private void reflesh(int fromMs, int toMs, boolean clear) {
		if (clear) monitorPanel.clear(toMs);
		// monitorPanel.drawBy(getShapeManager().getAnnotationDrawer(0, toMs));
		// monitorPanel.drawBy(getShapeManager().getLineDrawer(fromMs, toMs));
		// monitorPanel.drawBy(getShapeManager().getBorderDrawer());
		// monitorPanel.drawBy(getShapeManager().getSegDrawer());
		monitorPanel.drawBy(getShapeManager().getNextSegDrawer(toMs));
		monitorPanel.drawBy(getShapeManager().getFixationDrawer(toMs));
		monitorPanel.flush();
		
		this.toMs = toMs;
		this.fromMs = fromMs;
	}

	public void setActionLabel(Annotation aa) {
		timePanel.setActionLabel(aa);
	}
}
