package titech.manager;

import java.io.File;
import java.util.ArrayList;

import titech.AppConfig;
import titech.LogConfig;
import titech.loader.ActFileLoader;
import titech.loader.AdjFileLoader;
import titech.loader.EyeFileLoader;
import titech.loader.SegFileLoader;
import titech.loader.SnapsLoader;
import static titech.util.EasyGetter.*;

public class AppManager {
	
	private static final String DELIM = "-";
	
	private static final AppManager instance = new AppManager();
	
	private File logDir = null;
	private String dateStr = null;
	private String docName = null;
	private String userName = null;
	
	public AppManager() {
	}
	
	public static final AppManager instance() {
		return instance;
	}
	
	public void initialize(String logDirPath, boolean play) {
		setLogDir(logDirPath);
		
		File seg = AppConfig.SEG_LIST_FILE;
		File act = getAppManager().findFile("act");
		File eye = getAppManager().findFile("eye");
		File adj = getAppManager().findFile("adj");
		
		getAppManager().loadSegFile(seg);
		getAppManager().loadActFile(act);
		getAppManager().loadEyeFile(eye);
		if (play)
			getAppManager().loadAdjFile(adj);
		getAppManager().postProcess(play);
	}
	
	public File findFile(final String suffix) {
		for (File file : logDir.listFiles()) {
			if (file.getName().endsWith(suffix))
				return file;
		}
		return null;
	}
	
	public void postProcess(boolean play) {
		getTagManager().setLinks(getActionManager().linkResult());
		getActionManager().adjustTime();
		
		if (play) {
			getActionManager().calcFixations();
			getRegionManager().calcLineRegion();
			getRegionManager().calcSegRegion();
			getRegionManager().adjustFixations(getActionManager().getFixations());
			getActionManager().getAlteringAnnotations();
			getActionManager().calcWorkTime();
			getActionManager().listFixes();
			getLogManager().saveFixations(getActionManager().getFixations());
			getLogManager().saveWorkTime(getActionManager().getWorkTimes());
			getLogManager().saveFixesList(getActionManager().getFixesList());
		}
	}
	
	public void initShapeManager() {
		getShapeManager().setActions(ActionManager.instance().getAnnotations());
		getShapeManager().setTrackings(ActionManager.instance().getTrackings());
		getShapeManager().setFixations(ActionManager.instance().getFixations());
	}
	
	public String getSnapDirPath() {
		return logDir.getPath() + File.separator + AppConfig.SNAP_DIR_NAME;
	}
	
	public ArrayList<File> loadSnaps() {
		SnapsLoader loader = new SnapsLoader(new File(getSnapDirPath()));
		loader.parse();
		return loader.getSnaps();
	}
	
	private void loadActFile(File file) {
		ActFileLoader alparser = new ActFileLoader(file);
		alparser.parse();
		RegionManager.instance().setWidgetRegions(alparser.getWidgetRegions());
		RegionManager.instance().setCharRegions(alparser.getCharRegions());
		ActionManager.instance().setAnnotations(alparser.getActions());
	}
	
	private void loadEyeFile(File file) {
		EyeFileLoader etlparser = new EyeFileLoader(file);
		etlparser.parse();
		ActionManager.instance().setTrackings(etlparser.getTrackings());
	}
	
	private void loadSegFile(File file) {
		SegFileLoader slparser = new SegFileLoader(file);
		slparser.parse();
		TagManager.instance().setSegments(slparser.getSegments());
	}
	
	private void loadAdjFile(File file) {
		AdjFileLoader loader = new AdjFileLoader(file);
		loader.parse();
		getRegionManager().setLineStarts(loader.getLineStarts());
	}
	
	private void setLogDir(String logDirPath) {
		logDir = new File(logDirPath);
		
		LogConfig cfg = new LogConfig(logDirPath);
		
		dateStr = cfg.getDate();
		userName = cfg.getWhoid();
		docName = cfg.getDocid();
	}
	
	public String getDocName() {
		return docName;
	}
}
