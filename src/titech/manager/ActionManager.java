package titech.manager;

import java.util.ArrayList;
import java.util.TreeMap;
import java.util.TreeSet;

import titech.action.Action;
import titech.action.Annotation;
import titech.action.Glance;
import titech.action.Fixation;
import titech.action.Annotation.Kind;
import titech.analyzer.IDT;
import titech.annotation.Link;
import titech.ui.InfoPanel;
import titech.util.ExtArrayList;
import static titech.util.EasyGetter.*;

public class ActionManager {

	private static final ActionManager instance = new ActionManager();

	private long annotationStartMs;
	private long annotationEndMs;
	private long annotationDuration;

	private ExtArrayList<Glance> glances;
	private ExtArrayList<Annotation> annotations;
	private ExtArrayList<Annotation> alterings;
	private ExtArrayList<Fixation> fixations;
	
	private ArrayList<String> workTimes;
	private ArrayList<String> fixesList;

	public ActionManager() {
	}

	public final static ActionManager instance() {
		return instance;
	}
	
	public TreeMap<Integer, Link> linkResult() {
		TreeMap<Integer, Link> ret = new TreeMap<Integer, Link>();
		for (Annotation anno : annotations) {
			if (anno.getKind() == Annotation.Kind.create_link_end) {
				ret.put(anno.getTagId(), new Link(
						anno.getTagId(), anno.getTagName(),
						anno.getTagInfo1(), anno.getTagInfo2()));
			} else if (anno.getKind() == Annotation.Kind.delete_link) {
				ret.remove(anno.getTagId());
			}
		}
		return ret;
	}

	public void adjustTime() {
		annotationStartMs = annotations.get(0).getTime();
		Action.setBaseTime(annotationStartMs);
		annotationEndMs = annotations.get(annotations.size()-1).getTime();
		annotationDuration = annotationEndMs - annotationStartMs;
		
		int startTrimIndex = glances.upperBound(new Glance(annotationStartMs));
		int endTrimIndex = glances.upperBound(new Glance(annotationEndMs));

		ExtArrayList<Glance> newTrackings = new ExtArrayList<Glance>();

		// Trim tracking
		for (int i = startTrimIndex; i < endTrimIndex; ++i) {
			Glance tracking = glances.get(i);
			tracking.setTime(tracking.getTime() - annotationStartMs);
			if (newTrackings.size() == 0 && tracking.getTime() != 0)
				newTrackings.add(new Glance(0, -1, -1));
			newTrackings.add(tracking);
		}
		if (newTrackings.get(newTrackings.size()-1).getTime() != annotationDuration)
			newTrackings.add(new Glance(annotationDuration, -1, -1));

		// Renew each time stamp
		for (Annotation a : annotations)
			a.setTime(a.getTime() - annotationStartMs);

		glances = newTrackings;
	}
	
	public ArrayList<Annotation> getAlteringAnnotations() {
		ArrayList<Annotation> ret = new ArrayList<Annotation>();
		
		ret.add(annotations.get(0));
		for (Annotation a : annotations)
			if (a.isAltering()) ret.add(a);
		ret.add(annotations.get(annotations.size()-1));
		
		return ret;
	}
	
	public int getAnnoProgress(long time) {
		int i = annotations.lowerBound(new Annotation(time));
		Annotation anno = annotations.get(i);
		if (anno.getKind() == Annotation.Kind.create_seg_end ||
			anno.getKind() == Annotation.Kind.create_link_end) {
			
			Annotation start = annotations.get(i-1);
			Annotation end = anno;
			int duration = (int)(end.getTime() - start.getTime());
			int passed = (int)(time - start.getTime());
			
			return passed * InfoPanel.BAR_MAX_VAL / duration;
			
		} else {
			return 0;
		}
	}
	
	public Annotation getNextAltAnno(int time) {
		int next = alterings.upperBound(new Annotation(time));
		if (next >= alterings.size()) next = alterings.size()-1;
		
		return alterings.get(next);
	}
	
	public int getNextAnnoRatio(long time) {
		int p = alterings.lowerBoundDecl(new Annotation(time));
		Annotation prev = alterings.get(p);
		Annotation next = alterings.get(p + 1);
		int duration = (int)(next.getTime() - prev.getTime());
		int passed = (int)(time - prev.getTime());
		
		return passed * InfoPanel.BAR_MAX_VAL / duration;
	}
	
	// アノテーション時間中の停留リストを出力する
	// アノテーション時間が定義されていない場合，停留リストのサイズが0になる
	public void listFixes() {
		fixesList = new ArrayList<String>();
		
		ExtArrayList<Annotation> linkings = getLinkings();
		
		for (int ind = 0; ind < linkings.size(); ++ind) {
			if (linkings.get(ind).getKind() != Kind.create_link_start) continue;
			
			Annotation prev = linkings.get(ind - 1);
			Annotation start = linkings.get(ind);
			Annotation end = linkings.get(ind + 1);
			Fixation on_pred = getAfterFix(prev, start); // 別のリンク作成後，目的の述語への初めての停留
			
			int st = Integer.MAX_VALUE;
			if (on_pred != null) st = fixations.upperBound(on_pred) - 1;
			int ed = fixations.lowerBound(new Fixation(end.getTime()));
			
			ArrayList<String> a = new ArrayList<String>();
			
			a.add("**************************************************");
			a.add(start.toFormatStr());
			a.add(end.toFormatStr());
			for (int i = st; i < ed; ++i)
				a.add(fixations.get(i).toFormatStr());
			a.add("**************************************************");
			
			fixesList.addAll(a);
		}
	}
	
	public void calcWorkTime() {
		workTimes = new ArrayList<String>();
		
		ExtArrayList<Annotation> linkings = getLinkings();
		ArrayList<String> result = new ArrayList<String>();
		
		for (int ind = 0; ind < linkings.size(); ++ind) {
			if (linkings.get(ind).getKind() != Kind.create_link_start) continue;
			
			Annotation prev = linkings.get(ind - 1);
			Annotation start = linkings.get(ind);
			Annotation end = linkings.get(ind + 1);
			Annotation next = linkings.get(ind + 2);
			
			Fixation ff = getFirstFix(start);
			Fixation af = getAfterFix(prev, start);
			Fixation pf = getPrevFix(start);
			
			long firstFixStart_createStart = -1;
			long firstFixStart_createEnd = -1;
			if (ff != null) {
				firstFixStart_createStart = start.getTime() - ff.getTime();
				firstFixStart_createEnd = end.getTime() - ff.getTime();
				if (firstFixStart_createStart < 0) {
					firstFixStart_createStart = -1;
					firstFixStart_createEnd = -1;
				}
			}
			
			long prevFixStart_createStart = -1;
			long prevFixStart_createEnd = -1;
			if (pf != null) {
				prevFixStart_createStart = start.getTime() - pf.getTime();
				prevFixStart_createEnd = end.getTime() - pf.getTime();
				if (prevFixStart_createStart < 0) {
					prevFixStart_createStart = -1;
					prevFixStart_createEnd = -1;
				}
			}
			
			long afterFixStart_createStart = -1;
			long afterFixStart_createEnd = -1;
			if (af != null) {
				afterFixStart_createStart = start.getTime() - af.getTime();
				afterFixStart_createEnd = end.getTime() - af.getTime();
				if (afterFixStart_createStart < 0) {
					afterFixStart_createStart = -1;
					afterFixStart_createEnd = -1;
				}
			}
			
			long afterLinked_createStart = start.getTime() - prev.getTime();
			long afterLinked_createEnd = end.getTime() - prev.getTime();
			
//			String s = String.format(
//					"%s\t" + "%.2f\t" + "%.2f\t" + "%.2f\t" + "%.2f\t" + "%.2f\t" + "%.2f",
//					start.toFormatStr(),
//					firstFixStart_createStart == -1 ? 0.0 : firstFixStart_createStart / 1000.0,
//					firstFixStart_createEnd == -1 ? 0.0 : firstFixStart_createEnd / 1000.0,
//					prevFixStart_createStart == -1 ? 0.0 : prevFixStart_createStart / 1000.0,
//					prevFixStart_createEnd == -1 ? 0.0 : prevFixStart_createEnd / 1000.0,
//					afterFixStart_createStart == -1 ? 0.0 : afterFixStart_createStart / 1000.0,
//					afterFixStart_createEnd == -1 ? 0.0 : afterFixStart_createEnd / 1000.0);
			
			String s = String.format(
					"%s\t" + "%d," +"%d," +
					"%d," + "%d,"+
					"%d," + "%d," +
					"%d," + "%d",
					start.toFormatStr(),
					firstFixStart_createStart, firstFixStart_createEnd,
					prevFixStart_createStart, prevFixStart_createEnd,
					afterFixStart_createStart, afterFixStart_createEnd,
					afterLinked_createStart, afterLinked_createEnd);
			
//			System.out.println(s);
			workTimes.add(s);
		}
	}
	
	private ExtArrayList<Annotation> getLinkings() {
		ExtArrayList<Annotation> linkings = new ExtArrayList<Annotation>();
		for (Annotation a : annotations) {
			Kind kind = a.getKind();
			if (kind == Kind.annotation_start || kind == Kind.annotation_end ||
				kind == Kind.create_link_start || kind == Kind.create_link_end)
					linkings.add(a);
		}
		return linkings;
	}
	
	private ExtArrayList<Annotation> getFirstLinkings() {
		ExtArrayList<Annotation> linkings = new ExtArrayList<Annotation>();
		TreeSet<Integer> seenPreds = new TreeSet<Integer>();
		for (Annotation a : annotations) {
			Kind kind = a.getKind();
			if (kind == Kind.annotation_start || kind == Kind.annotation_end ||
				kind == Kind.create_link_start || kind == Kind.create_link_end ||
				kind == Kind.delete_link) {
			}
		}
		return linkings;
	}
	
	private Fixation getPrevFix(Annotation anno) {
		int predId = anno.getTagInfo2();
		long minDur = Long.MAX_VALUE;
		Fixation ret = null;
		for (Fixation f : fixations) {
			if (!f.onSeg()) continue;
			if (f.getSegRegion().getSeg().getId() == predId) {
				long dur = anno.getTime() - f.getTime();
				if (dur > 0 && dur < minDur) {
					dur = minDur;
					ret = f;
				}
			}
		}
		return ret;
	}
	
	private Fixation getFirstFix(Annotation anno) {
		int predId = anno.getTagInfo2();
		Fixation ret = null;
		for (Fixation f : fixations) {
			if (!f.onSeg()) continue;
			if (f.getSegRegion().getSeg().getId() == predId) {
				ret = f;
				break;
			}
		}
		return ret;
	}
	
	private Fixation getAfterFix(Annotation anno, Annotation next) {
		int predId = next.getTagInfo2();
		Fixation ret = null;
		for (Fixation f : fixations) {
			if (!f.onSeg()) continue;
			if (f.getSegRegion().getSeg().getId() == predId) {
				long dur = f.getTime() - anno.getTime();
				if (dur > 0) {
					ret = f;
					break;
				}
			}
		}
		return ret;
	}
	
	public boolean inErrorTracking(final int ms) {
		int i = glances.lowerBound(new Glance(ms));
		return glances.get(i).isError();
	}
	
	// Do NOT call BEFORE adjustEachTimeStamp() because of time stamp.
	public void calcFixations() {
		IDT idt = new IDT(glances);
		fixations = idt.calcFixations();
	}
	
	public int[] getFixationSection(long ms) {
		int from = fixations.lowerBound(new Fixation(ms));
		int to = fixations.upperBound(new Fixation(ms));
		return new int[] {from, to};
	}
	
	public int getFixationIndex(long start) {
		int ret = fixations.lowerBound(new Fixation(start));
		return ret;
	}

	public int getTrackingIndex(long msTimestamp) {
		int ret = glances.lowerBound(new Glance(msTimestamp));
		return ret;
	}

	public int getActionIndex(long msTimestamp) {
		int ret = annotations.lowerBound(new Annotation(msTimestamp));
		return ret;
	}

	public long getStartMs() {
		return glances.get(0).getTime();
	}

	public long getEndMs() {
		return glances.get(glances.size()-1).getTime();
	}

	//--------------------------------------------------
	// Getters and Setters

	public ExtArrayList<Annotation> getAnnotations() {
		return annotations;
	}

	public final ExtArrayList<Glance> getTrackings() {
		return glances;
	}

	public void setTrackings(ArrayList<Glance> eyeTrackings) {
		this.glances = new ExtArrayList<Glance>(eyeTrackings);
	}

	public void setAnnotations(ArrayList<Annotation> actions) {
		this.annotations = new ExtArrayList<Annotation>(actions);
		this.alterings = new ExtArrayList<Annotation>(getAlteringAnnotations());
	}

	public long getAnnoStartTime() {
		return annotationStartMs;
	}

	public long getAnnotationEndMs() {
		return annotationEndMs;
	}

	public long getAnnotationDuration() {
		return annotationDuration;
	}

	public ExtArrayList<Fixation> getFixations() {
		return fixations;
	}

	public void setFixations(ExtArrayList<Fixation> fixations) {
		this.fixations = fixations;
	}

	public ArrayList<String> getWorkTimes() {
		return workTimes;
	}

	public void setWorkTimes(ArrayList<String> workTimes) {
		this.workTimes = workTimes;
	}

	public ArrayList<String> getFixesList() {
		return fixesList;
	}

	public void setFixesList(ArrayList<String> fixesList) {
		this.fixesList = fixesList;
	}
}
