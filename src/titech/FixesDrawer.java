package titech;

import java.awt.BasicStroke;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;
import java.awt.geom.RoundRectangle2D;
import java.awt.image.BufferedImage;
import java.awt.Color;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.TreeMap;

class Item {
	int who, pcid;
	String pred, cas, arg;
	String uname, docid, snap_path;
	ArrayList<String> fix_segs = new ArrayList<String>();
	ArrayList<Integer> seg_cnts = new ArrayList<Integer>();
	ArrayList<String> fix_chars = new ArrayList<String>();
	ArrayList<Integer> char_cnts = new ArrayList<Integer>();
	boolean easy;
	int dist, time, atime;
}

public class FixesDrawer {
	
	int N = 3;
	ArrayList<Item>[] vis = new ArrayList[N];
	TreeMap<String, Shape> seg2rect;
	TreeMap<String, Shape> char2rect;
	TreeMap<String, String> cas2cute;
	
	public static void main(String[] args) {
		FixesDrawer main = new FixesDrawer();
		main.loadFixes("./dists.txt");
		main.loadSegs("./segs.txt");
		main.loadChars("./chars.txt");
		main.f();
		System.out.println("Finish.");
	}
	
	public FixesDrawer() {
		for (int i = 0; i < N; ++i) vis[i] = new ArrayList<Item>();
		seg2rect = new TreeMap<String, Shape>();
		char2rect = new TreeMap<String, Shape>();
		cas2cute = new TreeMap<String, String>() {{
			put("ga", "ガ格");
			put("o", "ヲ格");
			put("ni", "ニ格");
		}};
	}
	
	public void f() {
		for (int who = 0; who < N; ++who) {
			int pat1 = 0;
			int pat2 = 0;
			int pat3 = 0;
			int pat4 = 0;
			for (int n = 0; n < vis[who].size(); ++n) {
				Item item = vis[who].get(n);
				int pcid = item.pcid;
				BufferedImage image = loadImage(item.snap_path);
				for (int i = 0; i < item.fix_segs.size(); ++i)
					paintFixSeg(image, item, i);
				for (int i = 0; i < item.fix_chars.size(); ++i)
					paintFixChar(image, item, i);
				String pat = "";
				int patc = -1;
				if (item.easy && item.dist == 1) { pat = "1"; patc = pat1++; }
				if (!item.easy && item.dist == 1) { pat = "2"; patc = pat2++; }
				if (item.easy && item.dist != 1) { pat = "3"; patc =pat3++; }
				if (!item.easy && item.dist != 1) { pat = "4"; patc = pat4++; }
				String fname = String.format("A%d-Pat=%s-Id=%02d-Dist=%03d-ATime=%04.1f-Doc=%s-PTime=%04.0f.jpg",
						item.who, pat, patc, item.dist, item.atime/1000.0, item.docid, item.time/1000.0);
				System.out.println(fname + "\t" + item.who + "-" + item.docid + "-" + item.pred + "-" + item.cas + "-" + item.arg);
				saveImage(image, fname, "jpg");
				//break;
			}
			//break;
		}
	}
	
	public void paintFixSeg(BufferedImage image, Item item, int i) {
		String seg = item.fix_segs.get(i);
		RoundRectangle2D rect = (RoundRectangle2D)seg2rect.get(seg);
		int cnt = item.seg_cnts.get(i);
		String cas = item.cas;
		String pa = checkPA(item, seg);
		Color col = checkColor(item, seg);
		paint(image, rect, cnt, pa, col);
	}
	
	public void paintFixChar(BufferedImage image, Item item, int i) {
		String seg = item.fix_chars.get(i);
		RoundRectangle2D rect = (RoundRectangle2D)char2rect.get(seg);
		int cnt = item.char_cnts.get(i);
		String cas = item.cas;
		String pa = "";
		Color col = new Color(0, 0, 255);
		paint(image, rect, cnt, pa, col);
	}
	
	public Color checkColor(Item item, String seg) {
		if (seg.equals(item.pred) || seg.equals(item.arg))
			return new Color(255, 0, 0);
		else
			return new Color(0, 0, 255);
	}
	
	public String checkPA(Item item, String seg) {
		if (seg.equals(item.pred))
			return cas2cute.get(item.cas);
		else
			return "";
	}
	
	public void paint(BufferedImage image, RoundRectangle2D rect, int cnt, String pa, Color col) {
		Graphics2D g = image.createGraphics();
		BasicStroke stroke = new BasicStroke(3.0f);
		int nx = (int)rect.getX();
		int ny = (int)rect.getY() - 3;
		int cx = (int)rect.getX();
		int cy = (int)(rect.getY() + rect.getHeight() + 15);
		
	    g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g.setStroke(stroke);
		g.setPaint(col);
		g.draw(rect);
		g.drawString(cnt+"回", nx, ny);
		if (!pa.isEmpty()) g.drawString(pa, cx, cy);
		g.drawImage(image, 0, 0, null);
	}
	
	public void paint(BufferedImage image, Shape shape, Color col) {
		Graphics2D g = image.createGraphics();
		BasicStroke stroke = new BasicStroke(3.0f);
		
	    g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g.setStroke(stroke);
		g.setPaint(col);
		g.draw(shape);
		g.drawImage(image, 0, 0, null);
	}
	
	public BufferedImage loadImage(String path) {
		BufferedImage image = null;
		try {
			image = ImageIO.read(new File(path));
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(-1);
		}
		return image;
	}
	
	public void saveImage(BufferedImage image, String path, String suffix) {
		try {
			ImageIO.write(image, "jpg", new File(path));
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(-1);
		}
	}
	
	private void loadFixes(String path) {
		try {
			BufferedReader br = new BufferedReader(new FileReader(path));
			String line;
			while ((line = br.readLine()) != null) {
				Item i = new Item();
				String cols[] = line.split("\t");
				
				String[] item = cols[0].split("#");
				i.who = Integer.parseInt(item[0]);
				i.pcid = Integer.parseInt(item[1]);
				i.cas = item[2].split("_")[0];
				i.pred = item[2].split("_")[1];
				i.arg = item[3];
				
				item = cols[1].split("#");
				i.uname = item[0];
				i.docid = item[1];
				i.snap_path = item[2];
				
				item = cols[2].split("#");
				for (String s : item) {
					i.fix_segs.add(s.split(":")[0]);
					i.seg_cnts.add(Integer.parseInt(s.split(":")[1]));
				}
				
				if (!cols[3].isEmpty()) {
					item = cols[3].split("#");
					for (String s : item) {
						i.fix_chars.add(i.docid + ":" + s.split(":")[0]);
						i.char_cnts.add(Integer.parseInt(s.split(":")[1]));
					}
				}
				
				item = cols[4].split("#");
				i.easy = item[0].equals("1");
				i.dist = Integer.parseInt(item[1]);
				i.time = Integer.parseInt(item[2]);
				i.atime = Integer.parseInt(item[3]);
				
				vis[i.who].add(i);
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(-1);
		}
	}
	
	private void loadSegs(String path) {
		try {
			BufferedReader br = new BufferedReader(new FileReader(path));
			String line;
			while ((line = br.readLine()) != null) {
				String cols[] = line.split("\t");
				String seg = cols[0];
				String[] cords = cols[1].split(",");
				int x = Integer.parseInt(cords[0]);
				int y = Integer.parseInt(cords[1]);
				int w = Integer.parseInt(cords[2]);
				int h = Integer.parseInt(cords[3]);
				
				seg2rect.put(seg, new RoundRectangle2D.Double(x-2, y-2, w+2, h+4, 8, 8));
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(-1);
		}
	}
	
	private void loadChars(String path) {
		try {
			BufferedReader br = new BufferedReader(new FileReader(path));
			String line;
			while ((line = br.readLine()) != null) {
				String cols[] = line.split("\t");
				String ch = cols[0];
				String[] cords = cols[2].split(",");
				int x = Integer.parseInt(cords[0]);
				int y = Integer.parseInt(cords[1]);
				int w = Integer.parseInt(cords[2]);
				int h = Integer.parseInt(cords[3]);
				
				char2rect.put(ch, new RoundRectangle2D.Double(x-2, y-2, w+2, h+4, 8, 8));
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(-1);
		}
	}
}

class GPanel extends JPanel {
	
	@Override
	public void paintComponent(Graphics g) {
		paintComponent2(g);
	}
	
	public void paintComponent1(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2 = (Graphics2D)g;
		int w = this.getWidth();
		int h = this.getHeight();
		for (int i = 0; i < 10; ++i) {
			Ellipse2D shape = new Ellipse2D.Double(0, 0, w, h-i*(w/10));
			g2.setPaint(new Color(0, 0, 255, 25));
			g2.fill(shape);
		}
	}
	
	public void paintComponent2(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2 = (Graphics2D)g;
		int w = this.getWidth();
		int h = this.getHeight();
		for (int i = 0; i < 10; ++i) {
			Ellipse2D shape = new Ellipse2D.Double(i*(w/20), i*(h/20), w-i*(w/10), h-i*(h/10));
			g2.setPaint(new Color(0, 0, 255, 25));
			g2.fill(shape);
			g2.setPaint(Color.RED);
			g2.setStroke(new BasicStroke(i));
			g2.draw(shape);
		}
	}
}
