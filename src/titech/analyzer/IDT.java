package titech.analyzer;

import titech.action.Glance;
import titech.action.Fixation;
import titech.util.ExtArrayList;
import static titech.util.EasyGetter.*;

public class IDT {
	
	// Parameters for pseudo level I-DT algorithm.
	private int dispersionThreshPx = 16;
	private int durationThreshMs = 100;
	
	private ExtArrayList<Glance> trackings;

	public IDT(ExtArrayList<Glance> trackings) {
		this.trackings = trackings;
	}
	
	private boolean hasError(int st, int ed) {
		for (int i = st; i < ed; ++i)
			if (trackings.get(i).isError()) return true;
		return false;
	}
	
	private int[] calcCentroid(int st, int ed) {
		int sx = 0, sy = 0;
		int n = ed - st;
		for (int i = st; i < ed; ++i) {
			Glance t = trackings.get(i);
			if (t.isError()) continue;
			sx += t.getX();
			sy += t.getY();
		}
		return new int[] {sx/n, sy/n};
	}
	
	private boolean inDispersionThresh(int st, int ed) {
		if (hasError(st, ed)) return false;
		
		final int[] c = calcCentroid(st, ed);
		for (int i = st; i < ed; ++i) {
			final int x = trackings.get(i).getX() - c[0];
			final int y = trackings.get(i).getY() - c[1];
			if (!(Math.sqrt(x*x + y*y) <= dispersionThreshPx))
				return false;
		}
		return true;
	}
	
	private ExtArrayList<Fixation> mergeFixations(ExtArrayList<Fixation> fixations) {
		final ExtArrayList<Fixation> merged = new ExtArrayList<Fixation>();
		merged.add(fixations.get(0));
		
		for (int i = 1; i < fixations.size(); ++i) {
			final Fixation f = merged.get(merged.size()-1);
			final Fixation nf = fixations.get(i);
			final int x = nf.getX() - f.getX();
			final int y = nf.getY() - f.getY();
			if (nf.getTime() - f.getEndTime() < durationThreshMs &&
					Math.sqrt(x*x + y*y) <= dispersionThreshPx) {
				merged.remove(merged.size()-1);
				merged.add(new Fixation(f, nf));
			} else {
				merged.add(nf);
			}
		}
		return merged;
	}
	
	private void setErrorCount(Fixation f) {
		long st = f.getTime();
		long ed = st + f.getDuration();
		int stIndex = getActionManager().getTrackingIndex(st);
		int edIndex = getActionManager().getTrackingIndex(ed);
		int cnt = 0;
		for (int i = stIndex; i < edIndex; ++i)
			if (getActionManager().getTrackings().get(i).isError())
				cnt++;
		f.setErrPoints(cnt);
	}
	
	private ExtArrayList<Fixation> filterFixations(ExtArrayList<Fixation> fixations) {
		ExtArrayList<Fixation> filtered = new ExtArrayList<Fixation>();
		
		for (Fixation f : fixations)
			if (f.getDuration() >= durationThreshMs) {
				setErrorCount(f);
				filtered.add(f);
			}
		
		return filtered;
	}
	
	public ExtArrayList<Fixation> calcFixations() {
		final ExtArrayList<Fixation> fixations = new ExtArrayList<Fixation>();
		
		int st = 0;
		int ed = st + 1; // This is NOT inclusive.
		
		while (ed < trackings.size()) {
			if (inDispersionThresh(st, ed)) {
				do {
					if (ed+1 > trackings.size()) { ed = trackings.size()-1; break; }
					if (inDispersionThresh(st, ed+1)) ed++;
					else break;
				} while (true);
				
				final long ts = trackings.get(st).getTime();
				final long dur = trackings.get(ed).getTime() - trackings.get(st).getTime();
				final int[] cent = calcCentroid(st, ed);
				// Attention: N != end-start+1 because end is NOT inclusive.
				fixations.add(new Fixation(ts, dur, cent[0], cent[1], ed-st));
				st = ed;
				ed = st + 1;
				
			} else {
				st++;
				ed = st + 1;
			}
		}
		
		return filterFixations(mergeFixations(fixations));
	}
}
