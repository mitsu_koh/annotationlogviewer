package titech.util;

import java.util.ArrayList;
import java.util.Collection;

public class ExtArrayList<T extends Comparable<? super T>> extends ArrayList<T> {

	private static final long serialVersionUID = 1L;
	
	public ExtArrayList() {
		super(10);
	}
	
	public ExtArrayList(Collection<? extends T> c) {
		super(c);
	}
	
	public ExtArrayList(int initialCapacity) {
		super(initialCapacity);
	}
	
	public int lowerBound(T key) {
		int lo = -1, hi = size();
		while (hi - lo > 1) {
			int mid = (hi + lo) / 2;
			if (get(mid).compareTo(key) >= 0) {
				hi = mid;
			} else {
				lo = mid;
			}
		}
		return hi;
	}
	
	public int lowerBoundDecl(T key) {
		int ret = lowerBound(key);
		if (ret > 0) ret--;
		return ret;
	}
	
	public int upperBound(T key) {
		int lo = -1, hi = size();
		while (hi - lo > 1) {
			int mid = (hi + lo) / 2;
			if (get(mid).compareTo(key) <= 0) {
				lo = mid;
			} else {
				hi = mid;
			}
		}
		return hi;
	}
}
