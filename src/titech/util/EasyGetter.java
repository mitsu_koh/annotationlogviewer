package titech.util;

import titech.manager.ActionManager;
import titech.manager.AppManager;
import titech.manager.LogManager;
import titech.manager.RegionManager;
import titech.manager.ShapeManager;
import titech.manager.TagManager;
import titech.manager.UiManager;

public class EasyGetter {
	
	public static ActionManager getActionManager() {
		return ActionManager.instance();
	}
	
	public static AppManager getAppManager() {
		return AppManager.instance();
	}
	
	public static LogManager getLogManager() {
		return LogManager.instance();
	}
	
	public static RegionManager getRegionManager() {
		return RegionManager.instance();
	}
	
	public static ShapeManager getShapeManager() {
		return ShapeManager.instance();
	}
	
	public static TagManager getTagManager() {
		return TagManager.instance();
	}
	
	public static UiManager getUiManager() {
		return UiManager.instance();
	}
}
