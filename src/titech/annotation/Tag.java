package titech.annotation;

public abstract class Tag {

	protected int id;
	protected String tagName;

	public Tag(int id, String tagName) {
		this.id = id;
		this.tagName = tagName;
	}

	public int getId() {
		return id;
	}

	public String getTagName() {
		return tagName;
	}
}
