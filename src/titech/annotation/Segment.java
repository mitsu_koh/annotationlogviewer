package titech.annotation;

public class Segment extends Tag {
	
	public static final String PRED = "pred";
	public static final String NOUN = "noun";

	private int startIndex;
	private int endIndex;
	private String content;

	public Segment(int id, String tagName, int startIndex, int endIndex, String content) {
		super(id, tagName);
		this.startIndex = startIndex;
		this.endIndex = endIndex;
		this.content = content;
	}
	
	@Override
	public String toString() {
		return content + "(" + id + ")";
	}
	
	public String toFormatStr() {
		String ret = "";
		ret += id + ",";
		ret += tagName + ",";
		ret += startIndex + ",";
		ret += endIndex + ",";
		ret += content;
		return ret;
	}

	public int getStartIndex() {
		return startIndex;
	}

	public int getEndIndex() {
		return endIndex;
	}

	public String getContent() {
		return content;
	}

	public void setStartIndex(int startIndex) {
		this.startIndex = startIndex;
	}

	public void setEndIndex(int endIndex) {
		this.endIndex = endIndex;
	}

	public void setContent(String content) {
		this.content = content;
	}
}
