package titech.annotation;

import java.util.HashMap;

public class Link extends Tag {
	
	private final static HashMap<String, String> prettyName =
		new HashMap<String, String>() {{
			put("ga", "ガ");
			put("o", "ヲ");
			put("ni", "ニ");
		}};

	private int sourceId;
	private int destinationId;

	public Link(int id, String tagName, int sourceId, int destinationId) {
		super(id, tagName);
		this.sourceId = sourceId;
		this.destinationId = destinationId;
	}

	public int getSourceId() {
		return sourceId;
	}

	public int getDestinationId() {
		return destinationId;
	}
	
	public static String toPrettyName(String tagName) {
		return prettyName.get(tagName);
	}
}
