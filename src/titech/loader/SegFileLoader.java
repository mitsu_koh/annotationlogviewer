package titech.loader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;

import titech.annotation.Segment;
import static titech.util.EasyGetter.*;

public class SegFileLoader extends FileLoader {
	
	public static final String[] TAGDEF =
		new String[] { null, Segment.NOUN, Segment.PRED };
	
	private ArrayList<Segment> segments;

	public SegFileLoader(File file) {
		super(file);
		segments = new ArrayList<Segment>();
	}

	@Override
	public void parse() {
		try {
			FileReader fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);
			String line = "";
			
			while ((line = br.readLine()) != null) {
				Segment seg = parseLine(line);
				if (seg == null) {
					if (segments.size() > 0) break;
				} else {
					segments.add(seg);
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}
	}
	
	public Segment parseLine(final String line) {
		final String c[] = line.split("\t");
		
		final int id = Integer.parseInt(c[0]);
		final String tag = TAGDEF[Integer.parseInt(c[1])];
		final int st = Integer.parseInt(c[2]);
		final int ed = Integer.parseInt(c[3]);
		final String desc = c[4].substring(0, ed-st);
		String doc = c[5];
		
		if (doc.equals(getAppManager().getDocName())) {
			return new Segment(id, tag, st, ed, desc);
		} else {
			return null;
		}
	}

	public ArrayList<Segment> getSegments() {
		return segments;
	}
}
