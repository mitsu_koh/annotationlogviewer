package titech.loader;

import java.io.File;
import java.util.ArrayList;

public class SnapsLoader extends FileLoader {
	
	ArrayList<File> snaps = new ArrayList<File>();

	public SnapsLoader(File file) {
		super(file);
	}

	@Override
	public void parse() {
		for (File f : file.listFiles())
			if (f.getName().endsWith("jpg"))
				parse(f);
	}
	
	public void parse(File snap) {
		snaps.add(snap);
	}
	
	public ArrayList<File> getSnaps() {
		return snaps;
	}
}
