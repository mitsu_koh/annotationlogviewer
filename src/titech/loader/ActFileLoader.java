package titech.loader;

import java.io.*;
import java.util.*;

import titech.AppConfig;
import titech.action.Annotation;
import titech.region.CharRegion;
import titech.region.WidgetRegion;

public class ActFileLoader extends FileLoader {

	private static final String SIGN_ANNOTATION_DESCRIPTION = "@ANNOTATION_DESCRIPTION";
	private static final String SIGN_ACT_FILE_DESCRIPTION = "@ACT_FILE_DESCRIPTION";
	private static final String SIGN_PANEL_COORDINATE = "@PANEL_COORDINATE";
	private static final String SIGN_CHARACTER_COORDINATE = "@CHARACTER_COORDINATE";
	private static final String SIGN_ANNOTATION_ACTION = "@ANNOTATION_ACTION";

	private ArrayList<WidgetRegion> widgetRegions;
	private TreeSet<CharRegion> charRegions;
	private ArrayList<Annotation> actions;

	public ActFileLoader(File file) {
		super(file);
		widgetRegions = new ArrayList<WidgetRegion>();
		charRegions = new TreeSet<CharRegion>();
		actions = new ArrayList<Annotation>();
	}

	public void parse() {
		try {
			FileReader fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);

			String line;
			// Parse annotation description
			while ((line = br.readLine()) != null) {
				if (line.equals(SIGN_ACT_FILE_DESCRIPTION)) break;
			}

			// Parse act description
			while ((line = br.readLine()) != null) {
				if (line.equals(SIGN_PANEL_COORDINATE)) break;
			}

			// Parse widget positions
			while ((line = br.readLine()) != null) {
				if (line.equals(SIGN_CHARACTER_COORDINATE)) break;
				widgetRegions.add(parseWidgetPosition(line));
			}
			// Parse character positions
			while ((line = br.readLine()) != null) {
				if (line.equals(SIGN_ANNOTATION_ACTION)) break;
				CharRegion cr = parseCharPosition(line);
				charRegions.add(cr);
			}
			// Parse annotation actions
			while ((line = br.readLine()) != null) {
				if (line.startsWith("#")) {
//					if (!line.equals("#"))
//						System.out.println("info: " + line);
					continue;
				}
				actions.add(parseAnnotationAction(line));
			}

		} catch (Exception e) {
			e.printStackTrace();
			System.exit(-1);
		}
	}

	public WidgetRegion parseWidgetPosition(String line) {
		String[] a = line.split("\t");
		if (!a[0].equals("*"));
		String name = a[1];
		ArrayList<Integer> p = new ArrayList<Integer>();
		for (String s : a[2].split(","))
			p.add(Integer.parseInt(s));
		return new WidgetRegion(name, p.get(0), p.get(1), p.get(2), p.get(3));
	}

	public CharRegion parseCharPosition(String line) {
		String[] a = line.split("\t");
		int index = Integer.parseInt(a[0]);
		char c = a[1].charAt(0);
		ArrayList<Integer> p = new ArrayList<Integer>();
		for (String s : a[2].split(","))
			p.add(Integer.parseInt(s));
		return new CharRegion(
				index, c, p.get(0), p.get(1), p.get(2), p.get(3));
	}

	public Annotation parseAnnotationAction(String line) {
		String[] a = line.split("\t");

		// complete suffix
		while (a[0].length() < 10) a[0] += "0";
		while (a[0].length() < 11) a[0] += ".";
		while (a[0].length() < 14) a[0] += "0";
		int t = a[0].indexOf('.');
		String timestamp = a[0].substring(0, t) + a[0].substring(t+1);

		String actionName = a[1];

		String[] info = a[2].split(",");
		String tagId = info[0];
		String tagName = info[1];
		String tagInfo1 = info[2];
		String tagInfo2 = info[3];
		String tagContent = info[4];
		String actionInfo = info[5];

		return new Annotation(timestamp, actionName, tagId,
				tagName, tagInfo1, tagInfo2, tagContent, actionInfo);
	}

	//--------------------------------------------------
	// Getters

	public ArrayList<WidgetRegion> getWidgetRegions() {
		return widgetRegions;
	}

	public TreeSet<CharRegion> getCharRegions() {
		return charRegions;
	}

	public ArrayList<Annotation> getActions() {
		return actions;
	}
}
