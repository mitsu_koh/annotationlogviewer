package titech.loader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import titech.AppConfig;
import titech.action.Glance;
import titech.manager.ActionManager;

public class EyeFileLoader extends FileLoader {

	private static final String SIGN_ANNOTATION_DESCRIPTION = "@ANNOTATION_DESCRIPTION";
	private static final String SIGN_EYE_FILE_DESCRIPTION = "@EYE_FILE_DESCRIPTION";
	private static final String SIGN_EYE_TRACKING = "@EYE_TRACKING";

	private ArrayList<Glance> trackings;

	public EyeFileLoader(File file) {
		super(file);
	}

	public void parse() {
		trackings = new ArrayList<Glance>();

		try {
			FileReader fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);
			String line = "";

			// Parse annotation description
			while ((line = br.readLine()) != null) {
				if (line.equals(SIGN_ANNOTATION_DESCRIPTION)) break;
			}

			// Parse eye file description
			while ((line = br.readLine()) != null) {
				if (line.equals(SIGN_EYE_TRACKING)) break;
			}

			// Parse eye tracking
			while ((line = br.readLine()) != null) {
				Glance t = parseLogLine(line);
				trackings.add(t);
			}

		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}
	}

	public Glance parseLogLine(String line) {
		String[] colums = line.split("\t");

		int t = colums[0].indexOf('.');
		long timestamp = Long.parseLong(colums[0].substring(0, t) + colums[0].substring(t+1));

		String info[] = colums[1].split(",");
		int x = Integer.parseInt(info[0]);
		int y = Integer.parseInt(info[1]);

		return new Glance(timestamp, x, y);
	}

	//--------------------------------------------------
	// Getter

	public ArrayList<Glance> getTrackings() {
		return trackings;
	}
}
