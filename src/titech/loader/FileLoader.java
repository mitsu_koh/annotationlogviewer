package titech.loader;

import java.io.File;

abstract class FileLoader {

	File file;

	public FileLoader(File file) {
		this.file = file;
	}
	
	public abstract void parse();

	public File getFileName() {
		return file;
	}
}
