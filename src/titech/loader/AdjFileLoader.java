package titech.loader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;

import titech.AppConfig;

public class AdjFileLoader extends FileLoader {
	
	ArrayList<Integer> lineStarts;
	
	public AdjFileLoader(File file) {
		super(file);
		lineStarts = new ArrayList<Integer>();
	}

	@Override
	public void parse() {
		try {
			FileReader fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);
			String line = "";
			
			while ((line = br.readLine()) != null) {
				if (line.startsWith("#")) {
//					System.out.println(line);
					continue;
				} else {
					lineStarts.add(parseLine(line));
				}
			}
			
			lineStarts.add(AppConfig.TOBII_HEIGHT_PX);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public int parseLine(String s) {
		return Integer.parseInt(s);
	}
	
	public ArrayList<Integer> getLineStarts() {
		return lineStarts;
	}
}
